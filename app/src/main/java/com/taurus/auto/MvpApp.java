package com.taurus.auto;

import android.app.Application;

import androidx.room.Room;

import com.taurus.auto.data.db.AppDatabase;
import com.taurus.auto.di.component.AppComponent;
import com.taurus.auto.di.component.DaggerAppComponent;
import com.taurus.auto.di.module.ApplicationModule;

public class MvpApp extends Application {

    private AppComponent appComponent;
    public static MvpApp instance;
    private AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        appComponent.inject(this);
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static MvpApp getInstance() {
        return instance;
    }

    public AppDatabase getDatabase() {
        return database;
    }
}
