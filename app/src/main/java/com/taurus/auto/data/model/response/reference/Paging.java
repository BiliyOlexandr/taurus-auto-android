package com.taurus.auto.data.model.response.reference;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Paging {

    @SerializedName("previous_iterator")
    @Expose
    private Integer previousIterator;

    @SerializedName("next_iterator")
    @Expose
    private Integer nextIterator;

    public Integer getPreviousIterator() {
        return previousIterator;
    }

    public Integer getNextIterator() {
        return nextIterator;
    }
}
