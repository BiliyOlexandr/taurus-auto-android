package com.taurus.auto.data.model.response.profileinfo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoticeSetting implements Parcelable {

    @SerializedName("push_notice_enabled")
    @Expose
    private boolean pushNoticeEnabled;

    @SerializedName("email_notice_enabled")
    @Expose
    private boolean emailNoticeEnabled;

    protected NoticeSetting(Parcel in) {
        pushNoticeEnabled = in.readByte() != 0;
        emailNoticeEnabled = in.readByte() != 0;
    }

    public static final Creator<NoticeSetting> CREATOR = new Creator<NoticeSetting>() {
        @Override
        public NoticeSetting createFromParcel(Parcel in) {
            return new NoticeSetting(in);
        }

        @Override
        public NoticeSetting[] newArray(int size) {
            return new NoticeSetting[size];
        }
    };

    public boolean isPushNoticeEnabled() {
        return pushNoticeEnabled;
    }

    public boolean isEmailNoticeEnabled() {
        return emailNoticeEnabled;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (pushNoticeEnabled ? 1 : 0));
        dest.writeByte((byte) (emailNoticeEnabled ? 1 : 0));
    }
}
