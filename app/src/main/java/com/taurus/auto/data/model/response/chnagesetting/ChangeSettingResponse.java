package com.taurus.auto.data.model.response.chnagesetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taurus.auto.data.model.response.changepassword.Data;
import com.taurus.auto.data.model.response.registerphone.Errors;

import java.util.List;

public class ChangeSettingResponse {

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("data")
    @Expose
    private Data data;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("errors")
    @Expose
    private List<Errors> errorsList;

    public String getResult() {
        return result;
    }

    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public List<Errors> getErrorsList() {
        return errorsList;
    }
}
