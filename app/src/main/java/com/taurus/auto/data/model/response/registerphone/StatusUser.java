package com.taurus.auto.data.model.response.registerphone;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusUser implements Parcelable {

    @SerializedName("has_email")
    @Expose
    private boolean hasEmail;

    @SerializedName("has_phone")
    @Expose
    private boolean hasPhone;

    @SerializedName("has_country")
    @Expose
    private boolean hasCountry;

    @SerializedName("active")
    @Expose
    private boolean active;

    protected StatusUser(Parcel in) {
        hasEmail = in.readByte() != 0;
        hasPhone = in.readByte() != 0;
        hasCountry = in.readByte() != 0;
        active = in.readByte() != 0;
    }

    public static final Creator<StatusUser> CREATOR = new Creator<StatusUser>() {
        @Override
        public StatusUser createFromParcel(Parcel in) {
            return new StatusUser(in);
        }

        @Override
        public StatusUser[] newArray(int size) {
            return new StatusUser[size];
        }
    };

    public boolean isHasEmail() {
        return hasEmail;
    }

    public boolean isHasPhone() {
        return hasPhone;
    }

    public boolean isHasCountry() {
        return hasCountry;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (hasEmail ? 1 : 0));
        dest.writeByte((byte) (hasPhone ? 1 : 0));
        dest.writeByte((byte) (hasCountry ? 1 : 0));
        dest.writeByte((byte) (active ? 1 : 0));
    }
}
