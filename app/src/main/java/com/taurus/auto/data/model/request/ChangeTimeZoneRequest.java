package com.taurus.auto.data.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeTimeZoneRequest {

    @SerializedName("zone_id")
    @Expose
    private int zoneId;

    public ChangeTimeZoneRequest(int zoneId) {
        this.zoneId = zoneId;
    }
}
