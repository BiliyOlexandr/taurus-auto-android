package com.taurus.auto.data.model.response.registerphone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("access")
    @Expose
    private AccessToken accessToken;

    @SerializedName("status")
    @Expose
    private Status status;

    public Status getStatus() {
        return status;
    }

    public User getUser() {
        return user;
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }
}
