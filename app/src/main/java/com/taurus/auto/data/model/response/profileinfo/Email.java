package com.taurus.auto.data.model.response.profileinfo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Email implements Parcelable {

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("is_confirmed")
    @Expose
    private boolean isConfirmed;

    protected Email(Parcel in) {
        email = in.readString();
        isConfirmed = in.readByte() != 0;
    }

    public static final Creator<Email> CREATOR = new Creator<Email>() {
        @Override
        public Email createFromParcel(Parcel in) {
            return new Email(in);
        }

        @Override
        public Email[] newArray(int size) {
            return new Email[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeByte((byte) (isConfirmed ? 1 : 0));
    }
}
