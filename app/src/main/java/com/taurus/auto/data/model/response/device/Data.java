package com.taurus.auto.data.model.response.device;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("device_id")
    @Expose
    private String deviceId;

    @SerializedName("push_token")
    @Expose
    private String pushToken;

    public String getDeviceId() {
        return deviceId;
    }

    public String getPushToken() {
        return pushToken;
    }
}
