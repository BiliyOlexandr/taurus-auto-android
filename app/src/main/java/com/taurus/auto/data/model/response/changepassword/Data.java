package com.taurus.auto.data.model.response.changepassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("status")
    @Expose
    private Status status;

    public String getMessage() {
        return message;
    }

    public Status getStatus() {
        return status;
    }
}
