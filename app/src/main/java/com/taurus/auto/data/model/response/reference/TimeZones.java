package com.taurus.auto.data.model.response.reference;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeZones implements Parcelable {

    @SerializedName("zone_id")
    @Expose
    private int zoneId;

    @SerializedName("title")
    @Expose
    private String title;

    protected TimeZones(Parcel in) {
        zoneId = in.readInt();
        title = in.readString();
    }

    public static final Creator<TimeZones> CREATOR = new Creator<TimeZones>() {
        @Override
        public TimeZones createFromParcel(Parcel in) {
            return new TimeZones(in);
        }

        @Override
        public TimeZones[] newArray(int size) {
            return new TimeZones[size];
        }
    };

    public int getZoneId() {
        return zoneId;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(zoneId);
        dest.writeString(title);
    }
}
