package com.taurus.auto.data.network;

import android.util.Log;

import com.taurus.auto.utils.AppConstants;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {

    @Inject
    public RequestInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request request = original.newBuilder()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("X-Device-Id", "b93d9ff4d3f7e75c")
                .header("User-Agent", "Taurus-Auto/1.0 (Linux; U; Android 7.1.2; SM-N950N Build/NMF26X) Dalvik/2.1.0")
                .build();

        return chain.proceed(request);
    }
}
