package com.taurus.auto.data.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetPasswordRequest {

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("password")
    @Expose
    private String password;

    public ResetPasswordRequest(String phone, String code, String password) {
        this.phone = phone;
        this.code = code;
        this.password = password;
    }
}
