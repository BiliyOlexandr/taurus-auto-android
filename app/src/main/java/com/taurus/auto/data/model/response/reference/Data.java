package com.taurus.auto.data.model.response.reference;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("timezones")
    @Expose
    private List<TimeZones> timeZonesList;

    @SerializedName("groups")
    @Expose
    private List<Groups> groupsList;

    @SerializedName("paging_settings")
    @Expose
    private PagingSetting pagingSetting;

    @SerializedName("websocket_uri")
    @Expose
    private String websocketUri;

    public List<TimeZones> getTimeZonesList() {
        return timeZonesList;
    }

    public List<Groups> getGroupsList() {
        return groupsList;
    }

    public PagingSetting getPagingSetting() {
        return pagingSetting;
    }

    public String getWebsocketUri() {
        return websocketUri;
    }
}
