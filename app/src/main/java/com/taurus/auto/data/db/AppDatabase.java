package com.taurus.auto.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.taurus.auto.data.db.dao.ChatsDao;
import com.taurus.auto.data.model.response.listchat.Chats;

@Database(entities = {Chats.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ChatsDao chatsDao();
}