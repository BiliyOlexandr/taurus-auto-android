package com.taurus.auto.data.model.response.listchat;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

@Entity
public class Chats {

    @PrimaryKey
    @SerializedName("chat_id")
    @Expose
    private String chatId;

    @Embedded
    @SerializedName("last_message")
    @Expose
    private LastMessage lastMessage;

    @SerializedName("is_user_in_chat")
    @Expose
    private boolean isUserInChat;

    @SerializedName("unread_message_count")
    @Expose
    private int unreadMessageCount;

    @SerializedName("title")
    @Expose
    private String title;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public LastMessage getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(LastMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

    public boolean isUserInChat() {
        return isUserInChat;
    }

    public void setUserInChat(boolean userInChat) {
        isUserInChat = userInChat;
    }

    public int getUnreadMessageCount() {
        return unreadMessageCount;
    }

    public void setUnreadMessageCount(int unreadMessageCount) {
        this.unreadMessageCount = unreadMessageCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chats chats = (Chats) o;
        return isUserInChat == chats.isUserInChat &&
                unreadMessageCount == chats.unreadMessageCount &&
                Objects.equals(chatId, chats.chatId) &&
                Objects.equals(lastMessage, chats.lastMessage) &&
                Objects.equals(title, chats.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chatId, lastMessage, isUserInChat, unreadMessageCount, title);
    }
}
