package com.taurus.auto.data.model.response.registerphone;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User implements Parcelable {

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("groups")
    @Expose
    private List<Group> groupList;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("status")
    @Expose
    private StatusUser statusUser;

    protected User(Parcel in) {
        firstName = in.readString();
        groupList = in.createTypedArrayList(Group.CREATOR);
        id = in.readString();
        lastName = in.readString();
        statusUser = in.readParcelable(StatusUser.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public String getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public StatusUser getStatusUser() {
        return statusUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeTypedList(groupList);
        dest.writeString(id);
        dest.writeString(lastName);
        dest.writeParcelable(statusUser, flags);
    }
}
