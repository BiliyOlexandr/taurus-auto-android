package com.taurus.auto.data.model.response.mainmenu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("items")
    @Expose
    private List<ItemsMenu> itemsMenuList;

    public List<ItemsMenu> getItemsMenuList() {
        return itemsMenuList;
    }
}
