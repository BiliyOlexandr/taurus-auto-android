package com.taurus.auto.data.model.response.reference;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PagingSetting {

    @SerializedName("chat_paging")
    @Expose
    private Paging chatPaging;

    @SerializedName("chat_message_paging")
    @Expose
    private Paging chatMessagePaging;

    @SerializedName("user_paging")
    @Expose
    private Paging userPaging;

    public Paging getChatPaging() {
        return chatPaging;
    }

    public Paging getChatMessagePaging() {
        return chatMessagePaging;
    }

    public Paging getUserPaging() {
        return userPaging;
    }
}
