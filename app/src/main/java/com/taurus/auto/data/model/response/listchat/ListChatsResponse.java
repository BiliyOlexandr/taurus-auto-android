package com.taurus.auto.data.model.response.listchat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taurus.auto.data.model.response.registerphone.Errors;

import java.util.List;

public class ListChatsResponse {

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("data")
    @Expose
    private DataListChats dataListChats;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("errors")
    @Expose
    private List<Errors> errorsList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public DataListChats getDataListChats() {
        return dataListChats;
    }

    public void setDataListChats(DataListChats dataListChats) {
        this.dataListChats = dataListChats;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Errors> getErrorsList() {
        return errorsList;
    }

    public void setErrorsList(List<Errors> errorsList) {
        this.errorsList = errorsList;
    }
}
