package com.taurus.auto.data.model.response.userlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("has_email")
    @Expose
    private boolean hasEmail;

    @SerializedName("has_phone")
    @Expose
    private boolean hasPhone;

    @SerializedName("has_country")
    @Expose
    private boolean hasCountry;

    @SerializedName("active")
    @Expose
    private boolean active;

    public boolean isHasEmail() {
        return hasEmail;
    }

    public void setHasEmail(boolean hasEmail) {
        this.hasEmail = hasEmail;
    }

    public boolean isHasPhone() {
        return hasPhone;
    }

    public void setHasPhone(boolean hasPhone) {
        this.hasPhone = hasPhone;
    }

    public boolean isHasCountry() {
        return hasCountry;
    }

    public void setHasCountry(boolean hasCountry) {
        this.hasCountry = hasCountry;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
