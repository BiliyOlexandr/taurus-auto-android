package com.taurus.auto.data.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceRequest {

    @SerializedName("platform")
    @Expose
    private String platform;

    @SerializedName("device_id")
    @Expose
    private String deviceId;

    @SerializedName("push_token")
    @Expose
    private String pushToken;

    public DeviceRequest(String platform, String deviceId, String pushToken) {
        this.platform = platform;
        this.deviceId = deviceId;
        this.pushToken = pushToken;
    }
}
