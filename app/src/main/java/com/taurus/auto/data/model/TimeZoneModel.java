package com.taurus.auto.data.model;

public class TimeZoneModel {

    private String time;
    private String name;

    public TimeZoneModel(String time, String name) {
        this.time = time;
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public String getName() {
        return name;
    }
}
