package com.taurus.auto.data.model.response.mainmenu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemsMenu {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("title")
    @Expose
    private String title;

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }
}
