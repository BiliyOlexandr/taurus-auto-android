package com.taurus.auto.data.enums;

import com.taurus.auto.R;

public enum Menu {

    PLATFORMS("platforms", R.string.menu_platforms, R.drawable.ic_background_platforms_text, R.drawable.ic_chooseicon),
    CHATS("chats", R.string.menu_chat, R.drawable.ic_background_chat, R.drawable.ic_chatsicon),
    CHAT("chat", R.string.menu_chat, R.drawable.ic_background_chat, R.drawable.ic_chatsicon),
    FAQ("faq", R.string.menu_faq, R.drawable.ic_background_faq, R.drawable.ic_faqicon),
    RECOMMEND("recommend", R.string.menu_recommend, R.drawable.ic_background_recommend_client, R.drawable.ic_cellserviceicon),
    TEXTS("texts", R.string.menu_texts, R.drawable.ic_background_platforms_text, R.drawable.ic_texticon),
    USERS("users", R.string.menu_users, R.drawable.ic_background_recommend_client, R.drawable.ic_clientsicon);

    private final String value;
    private final int text;
    private final int idFon;
    private final int idImage;

    private Menu(String value, int text, int idFon, int idImage) {
        this.value = value;
        this.idFon = idFon;
        this.idImage = idImage;
        this.text = text;
    }

    public int getText() {
        return text;
    }

    public String getValue() {
        return value;
    }

    public int getIdFon() {
        return idFon;
    }

    public int getIdImage() {
        return idImage;
    }
}