package com.taurus.auto.data.model.response.listchat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taurus.auto.data.model.response.userlist.Paging;

import java.util.List;

public class DataListChats {

    @SerializedName("chats")
    @Expose
    private List<Chats> chatsList;

    @SerializedName("paging")
    @Expose
    private Paging paging;

    public List<Chats> getChatsList() {
        return chatsList;
    }

    public void setChatsList(List<Chats> chatsList) {
        this.chatsList = chatsList;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
