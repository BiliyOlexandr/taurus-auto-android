package com.taurus.auto.data.model.response.registerphone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("completion")
    @Expose
    private String completion;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("proceed_to")
    @Expose
    private String proceedTo;

    public String getCompletion() {
        return completion;
    }

    public String getMessage() {
        return message;
    }

    public String getProceedTo() {
        return proceedTo;
    }

    @Override
    public String toString() {
        return "Status{" +
                "completion='" + completion + '\'' +
                ", message='" + message + '\'' +
                ", proceedTo='" + proceedTo + '\'' +
                '}';
    }
}
