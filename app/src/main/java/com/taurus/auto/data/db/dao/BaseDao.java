package com.taurus.auto.data.db.dao;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

public interface BaseDao<T> {

    @Insert(onConflict = REPLACE)
    void insert(T onj);

    @Insert(onConflict = REPLACE)
    void insert(List<T> obj);

    @Update
    void update(T obj);

    @Delete
    void delete(T obj);
}
