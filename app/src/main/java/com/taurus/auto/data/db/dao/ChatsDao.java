package com.taurus.auto.data.db.dao;

import com.taurus.auto.data.model.response.listchat.Chats;

public interface ChatsDao extends BaseDao<Chats> {

}
