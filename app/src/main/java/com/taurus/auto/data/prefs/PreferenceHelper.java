package com.taurus.auto.data.prefs;

public interface PreferenceHelper {

    void setAccessToken(String accessToken);
    String getAccessToken();
}
