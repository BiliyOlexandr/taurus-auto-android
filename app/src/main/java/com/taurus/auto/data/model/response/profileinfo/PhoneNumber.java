package com.taurus.auto.data.model.response.profileinfo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneNumber implements Parcelable {

    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    @SerializedName("is_confirmed")
    @Expose
    private boolean isConfirmed;

    protected PhoneNumber(Parcel in) {
        phoneNumber = in.readString();
        isConfirmed = in.readByte() != 0;
    }

    public static final Creator<PhoneNumber> CREATOR = new Creator<PhoneNumber>() {
        @Override
        public PhoneNumber createFromParcel(Parcel in) {
            return new PhoneNumber(in);
        }

        @Override
        public PhoneNumber[] newArray(int size) {
            return new PhoneNumber[size];
        }
    };

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(phoneNumber);
        dest.writeByte((byte) (isConfirmed ? 1 : 0));
    }
}
