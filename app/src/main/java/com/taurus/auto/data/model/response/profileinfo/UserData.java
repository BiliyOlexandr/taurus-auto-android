package com.taurus.auto.data.model.response.profileinfo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taurus.auto.data.model.response.reference.TimeZones;

public class UserData implements Parcelable {

    @SerializedName("email")
    @Expose
    private Email email;

    @SerializedName("phone_number")
    @Expose
    private PhoneNumber phoneNumber;

    @SerializedName("notice_settings")
    @Expose
    private NoticeSetting noticeSetting;

    @SerializedName("timezone")
    @Expose
    private TimeZones timeZones;

    protected UserData(Parcel in) {
        email = in.readParcelable(Email.class.getClassLoader());
        phoneNumber = in.readParcelable(PhoneNumber.class.getClassLoader());
        noticeSetting = in.readParcelable(NoticeSetting.class.getClassLoader());
        timeZones = in.readParcelable(TimeZones.class.getClassLoader());
    }

    public static final Creator<UserData> CREATOR = new Creator<UserData>() {
        @Override
        public UserData createFromParcel(Parcel in) {
            return new UserData(in);
        }

        @Override
        public UserData[] newArray(int size) {
            return new UserData[size];
        }
    };

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public NoticeSetting getNoticeSetting() {
        return noticeSetting;
    }

    public Email getEmail() {
        return email;
    }

    public TimeZones getTimeZones() {
        return timeZones;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(email, flags);
        dest.writeParcelable(phoneNumber, flags);
        dest.writeParcelable(noticeSetting, flags);
        dest.writeParcelable(timeZones, flags);
    }
}
