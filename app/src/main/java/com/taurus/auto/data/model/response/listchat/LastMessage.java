package com.taurus.auto.data.model.response.listchat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class LastMessage {

    @SerializedName("date_time_utc")
    @Expose
    private int dateTimeUtc;

    @SerializedName("date_full")
    @Expose
    private String dateFull;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("message_id")
    @Expose
    private String messageId;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("date_time")
    @Expose
    private int dateTime;

    @SerializedName("status_text")
    @Expose
    private String statusText;

    @SerializedName("user")
    @Expose
    private User user;

    public int getDateTimeUtc() {
        return dateTimeUtc;
    }

    public void setDateTimeUtc(int dateTimeUtc) {
        this.dateTimeUtc = dateTimeUtc;
    }

    public String getDateFull() {
        return dateFull;
    }

    public void setDateFull(String dateFull) {
        this.dateFull = dateFull;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDateTime() {
        return dateTime;
    }

    public void setDateTime(int dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LastMessage that = (LastMessage) o;
        return dateTimeUtc == that.dateTimeUtc &&
                dateTime == that.dateTime &&
                Objects.equals(dateFull, that.dateFull) &&
                Objects.equals(text, that.text) &&
                Objects.equals(messageId, that.messageId) &&
                Objects.equals(date, that.date) &&
                Objects.equals(statusText, that.statusText) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTimeUtc, dateFull, text, messageId, date, dateTime, statusText, user);
    }
}
