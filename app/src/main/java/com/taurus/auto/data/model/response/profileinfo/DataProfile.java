package com.taurus.auto.data.model.response.profileinfo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taurus.auto.data.model.response.registerphone.User;

public class DataProfile implements Parcelable {

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("user_data")
    @Expose
    private UserData userData;

    protected DataProfile(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        userData = in.readParcelable(UserData.class.getClassLoader());
    }

    public static final Creator<DataProfile> CREATOR = new Creator<DataProfile>() {
        @Override
        public DataProfile createFromParcel(Parcel in) {
            return new DataProfile(in);
        }

        @Override
        public DataProfile[] newArray(int size) {
            return new DataProfile[size];
        }
    };

    public User getUser() {
        return user;
    }

    public UserData getUserData() {
        return userData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeParcelable(userData, flags);
    }
}
