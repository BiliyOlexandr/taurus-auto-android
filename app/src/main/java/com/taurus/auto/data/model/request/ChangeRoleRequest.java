package com.taurus.auto.data.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChangeRoleRequest {

    @SerializedName("groups")
    @Expose
    private List<String> groupsList;

    public ChangeRoleRequest(List<String> groupsList) {
        this.groupsList = groupsList;
    }
}
