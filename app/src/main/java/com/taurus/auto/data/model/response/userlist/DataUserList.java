package com.taurus.auto.data.model.response.userlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataUserList {

    @SerializedName("users")
    @Expose
    private List<User> userList;

    @SerializedName("paging")
    @Expose
    private Paging paging;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
