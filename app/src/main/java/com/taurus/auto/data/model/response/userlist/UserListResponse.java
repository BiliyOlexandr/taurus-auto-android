package com.taurus.auto.data.model.response.userlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taurus.auto.data.model.response.changepassword.Data;
import com.taurus.auto.data.model.response.registerphone.Errors;

import java.util.List;

public class UserListResponse {

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("data")
    @Expose
    private DataUserList data;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("errors")
    @Expose
    private List<Errors> errorsList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public DataUserList getData() {
        return data;
    }

    public void setData(DataUserList data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Errors> getErrorsList() {
        return errorsList;
    }

    public void setErrorsList(List<Errors> errorsList) {
        this.errorsList = errorsList;
    }
}
