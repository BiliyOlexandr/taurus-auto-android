package com.taurus.auto.data.model.response.changepassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("completion")
    @Expose
    private String completion;

    @SerializedName("message")
    @Expose
    private String message;

    public String getCompletion() {
        return completion;
    }

    public String getMessage() {
        return message;
    }
}
