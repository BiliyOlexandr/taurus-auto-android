package com.taurus.auto.data.model.response.registerphone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccessToken {

    @SerializedName("token")
    @Expose
    private String accessToken;

    @SerializedName("expire")
    @Expose
    private int expire;

    public String getAccessToken() {
        return accessToken;
    }

    public int getExpire() {
        return expire;
    }
}
