package com.taurus.auto.data.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeSettingRequest {

    @SerializedName("push_notice_enabled")
    @Expose
    private boolean pushNoticeEnabled;

    @SerializedName("email_notice_enabled")
    @Expose
    private boolean emailNoticeEnabled;

    public ChangeSettingRequest(boolean pushNoticeEnabled, boolean emailNoticeEnabled) {
        this.pushNoticeEnabled = pushNoticeEnabled;
        this.emailNoticeEnabled = emailNoticeEnabled;
    }
}
