package com.taurus.auto.data.network;

import com.taurus.auto.data.model.request.ChangeActiveRequest;
import com.taurus.auto.data.model.request.ChangePasswordRequest;
import com.taurus.auto.data.model.request.ChangeRoleRequest;
import com.taurus.auto.data.model.request.ChangeSettingRequest;
import com.taurus.auto.data.model.request.ChangeTimeZoneRequest;
import com.taurus.auto.data.model.request.LoginRequest;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.data.model.request.ResetPasswordRequest;
import com.taurus.auto.data.model.request.UpdateProfileDateRequest;
import com.taurus.auto.data.model.response.changepassword.ChangePasswordResponse;
import com.taurus.auto.data.model.response.chnagesetting.ChangeSettingResponse;
import com.taurus.auto.data.model.response.listchat.ListChatsResponse;
import com.taurus.auto.data.model.response.mainmenu.MainMenuResponse;
import com.taurus.auto.data.model.response.profileinfo.ProfileResponse;
import com.taurus.auto.data.model.response.reference.ReferenceResponse;
import com.taurus.auto.data.model.response.registerphone.RegisterPhoneResponse;
import com.taurus.auto.data.model.response.updateprofile.UpdateProfileDateResponse;
import com.taurus.auto.data.model.response.userlist.UserListResponse;
import com.taurus.auto.ui.activities.EditProfileActivity;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiServiceTaurusAuto {

    @POST("api/auth/register/phone")
    Single<Response<RegisterPhoneResponse>> registerPhone(@Body RegisterPhoneRequest registerPhoneRequest);

    @POST("api/auth/login")
    Single<Response<RegisterPhoneResponse>> login(@Body LoginRequest loginRequest);

    @POST("/api/auth/reset/password")
    Single<Response<RegisterPhoneResponse>> resetPassword(@Body ResetPasswordRequest resetPasswordRequest);

    @GET("/api/menu/show")
    Single<Response<MainMenuResponse>> getMainMenuInfo(@Header("X-Auth-Token") String token);

    @POST("/api/users/me/password")
    Single<Response<ChangePasswordResponse>> changePassword(@Header("X-Auth-Token") String token, @Body ChangePasswordRequest changePasswordRequest);

    @GET("/api/users/me")
    Single<Response<ProfileResponse>> getProfileInfo(@Header("X-Auth-Token") String token);

    @POST("/api/users/me/notice_settings")
    Single<Response<ChangeSettingResponse>> changeSetting(@Header("X-Auth-Token") String token, @Body ChangeSettingRequest changeSettingRequest);

    @GET("/api/reference")
    Single<Response<ReferenceResponse>> getReferences();

    @POST("/api/users/me/timezone")
    Single<Response<ChangePasswordResponse>> changeTimeZone(@Header("X-Auth-Token") String token, @Body ChangeTimeZoneRequest changeTimeZoneRequest);

    @POST("/api/users/me")
    Single<Response<UpdateProfileDateResponse>> updateDate(@Header("X-Auth-Token") String token, @Body UpdateProfileDateRequest updateProfileDateRequest);

    @POST("/api/users/me/phone")
    Single<Response<ChangePasswordResponse>> updatePhone(@Header("X-Auth-Token") String token, @Body RegisterPhoneRequest registerPhoneRequest);

    @GET("/api/users/list")
    Single<Response<UserListResponse>> getUserList(@Header("X-Auth-Token") String token, @Query("q") String search,
                                                   @Query("active") boolean active, @Query("limit") int limit, @Query("page") String page);

    @GET("/api/users/{userId}")
    Single<Response<ProfileResponse>> getUserDate(@Header("X-Auth-Token") String token, @Path("userId") String userId);

    @POST("/api/users/{userId}/activation")
    Single<Response<ChangeSettingResponse>> changeActiveUser(@Header("X-Auth-Token") String token, @Path("userId") String userId, @Body ChangeActiveRequest changeActiveRequest);

    @POST("/api/users/{userId}/groups")
    Single<Response<ChangeSettingResponse>> changeRole(@Header("X-Auth-Token") String token, @Path("userId") String userId, @Body ChangeRoleRequest changeRoleRequest);

    @GET("/api/chats")
    Single<Response<ListChatsResponse>> getListChats(@Header("X-Auth-Token") String token, @Query("page") String page);
}
