package com.taurus.auto.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.taurus.auto.di.ApplicationContext;
import com.taurus.auto.di.PreferenceInfo;

import javax.inject.Inject;

public class AppPreferenceHelper implements PreferenceHelper {

    private final SharedPreferences mPrefs;
    public static final String PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN";

    @Inject
    public AppPreferenceHelper(@ApplicationContext Context context, @PreferenceInfo String preferenceName){
        mPrefs = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_ACCESS_TOKEN, null);
    }
}
