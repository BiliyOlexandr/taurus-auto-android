package com.taurus.auto.utils;

public class AppConstants {

    public static final String PREF_NAME = "taurus_pref";

    public static final String TAG = "Taurus";

    public static final String BASE_URL = "http://taurus.inbgroup.com/";
}
