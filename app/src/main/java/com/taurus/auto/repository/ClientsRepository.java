package com.taurus.auto.repository;

import com.taurus.auto.data.model.request.ChangeActiveRequest;
import com.taurus.auto.data.model.request.ChangeRoleRequest;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.Groups;
import com.taurus.auto.data.model.response.userlist.DataUserList;

import java.util.List;

import io.reactivex.Single;

public interface ClientsRepository extends BaseRepository {

    Single<DataUserList> getClientsList(String search, boolean active, String page);

    Single<DataProfile> getUserDate(String userId);

    Single<String> changeActiveUser(ChangeActiveRequest changeActiveRequest, String userId);

    Single<String> changeRole(ChangeRoleRequest changeRoleRequest, String userId);

    Single<List<Groups>> getRole();
}
