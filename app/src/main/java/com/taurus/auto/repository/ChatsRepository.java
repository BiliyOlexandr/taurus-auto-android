package com.taurus.auto.repository;

import io.reactivex.Single;

public interface ChatsRepository extends BaseRepository {

    Single<String> getListChats(String page);
}
