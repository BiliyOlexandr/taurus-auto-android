package com.taurus.auto.repository;

import com.taurus.auto.data.network.ApiServiceTaurusAuto;
import com.taurus.auto.data.prefs.PreferenceHelper;

import javax.inject.Inject;

import io.reactivex.Single;

public class ChatsRepositoryImp extends BaseRepositoryImp implements ChatsRepository {

    @Inject
    ChatsRepositoryImp(ApiServiceTaurusAuto apiServiceTaurusAuto, PreferenceHelper preferenceHelper) {
        super(apiServiceTaurusAuto, preferenceHelper);
    }

    @Override
    public Single<String> getListChats(String page) {
        return getApiServiceAuto().getListChats(getPreferenceHelper().getAccessToken(), page)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just("ok");
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }
}
