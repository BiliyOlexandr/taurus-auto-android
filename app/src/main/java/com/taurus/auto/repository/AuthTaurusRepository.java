package com.taurus.auto.repository;

import com.taurus.auto.data.model.request.LoginRequest;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.data.model.request.ResetPasswordRequest;
import com.taurus.auto.data.model.response.registerphone.Data;
import com.taurus.auto.data.model.response.registerphone.RegisterPhoneResponse;

import io.reactivex.Single;

public interface AuthTaurusRepository extends BaseRepository {

    Single<String> registerPhone(RegisterPhoneRequest registerPhoneRequest);

    Single<String> checkPhone(RegisterPhoneRequest registerPhoneRequest);

    Single<String> login(LoginRequest loginRequest);

    Single<String> resetPasswordPhone(ResetPasswordRequest resetPasswordRequest);

    Single<String> resetPasswordCode(ResetPasswordRequest resetPasswordRequest);

    Single<RegisterPhoneResponse> resetPasswordNew(ResetPasswordRequest resetPasswordRequest);
}
