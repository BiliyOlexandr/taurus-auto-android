package com.taurus.auto.repository;

import com.taurus.auto.data.model.request.ChangePasswordRequest;
import com.taurus.auto.data.model.request.ChangeSettingRequest;
import com.taurus.auto.data.model.request.ChangeTimeZoneRequest;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.data.model.request.UpdateProfileDateRequest;
import com.taurus.auto.data.model.response.changepassword.ChangePasswordResponse;
import com.taurus.auto.data.model.response.mainmenu.ItemsMenu;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.TimeZones;

import java.util.List;

import io.reactivex.Single;

public interface ProfileRepository extends BaseRepository {

    Single<List<ItemsMenu>> getMainMenuInfo();

    Single<ChangePasswordResponse> changePassword(ChangePasswordRequest changePasswordRequest);

    Single<DataProfile> getProfileInfo();

    Single<String> changeSetting(ChangeSettingRequest changeSettingRequest);

    Single<List<TimeZones>> getTimezones();

    Single<String> changeTimeZone(ChangeTimeZoneRequest changeTimeZoneRequest);

    Single<String> updateDate(UpdateProfileDateRequest updateProfileDateRequest);

    Single<String> updatePhone(RegisterPhoneRequest registerPhoneRequest);
}
