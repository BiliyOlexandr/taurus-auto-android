package com.taurus.auto.repository;

import com.taurus.auto.data.network.ApiServiceTaurusAuto;
import com.taurus.auto.data.prefs.PreferenceHelper;
import com.taurus.auto.di.PreferenceInfo;

public interface BaseRepository {

    ApiServiceTaurusAuto getApiServiceAuto();

    PreferenceHelper getPreferenceHelper();

}
