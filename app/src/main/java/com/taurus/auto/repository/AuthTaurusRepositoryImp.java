package com.taurus.auto.repository;

import com.taurus.auto.data.model.request.LoginRequest;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.data.model.request.ResetPasswordRequest;
import com.taurus.auto.data.model.response.registerphone.RegisterPhoneResponse;
import com.taurus.auto.data.network.ApiServiceTaurusAuto;
import com.taurus.auto.data.prefs.PreferenceHelper;

import javax.inject.Inject;

import io.reactivex.Single;

public class AuthTaurusRepositoryImp extends BaseRepositoryImp implements AuthTaurusRepository {

    @Inject
    AuthTaurusRepositoryImp(ApiServiceTaurusAuto apiServiceTaurusAuto, PreferenceHelper preferenceHelper) {
        super(apiServiceTaurusAuto, preferenceHelper);
    }

    @Override
    public Single<String> registerPhone(RegisterPhoneRequest registerPhoneRequest) {
        return getApiServiceAuto().registerPhone(registerPhoneRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData().getStatus().getMessage());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> checkPhone(RegisterPhoneRequest registerPhoneRequest) {
        return getApiServiceAuto().registerPhone(registerPhoneRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                getPreferenceHelper().setAccessToken(response.body().getData().getAccessToken().getAccessToken());
                                return Single.just("success");
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> login(LoginRequest loginRequest) {
        return getApiServiceAuto().login(loginRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                getPreferenceHelper().setAccessToken(response.body().getData().getAccessToken().getAccessToken());
                                return Single.just("success");
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getField()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> resetPasswordPhone(ResetPasswordRequest resetPasswordRequest) {
        return getApiServiceAuto().resetPassword(resetPasswordRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData().getStatus().getMessage());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> resetPasswordCode(ResetPasswordRequest resetPasswordRequest) {
        return getApiServiceAuto().resetPassword(resetPasswordRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just("success");
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<RegisterPhoneResponse> resetPasswordNew(ResetPasswordRequest resetPasswordRequest) {
        return getApiServiceAuto().resetPassword(resetPasswordRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                getPreferenceHelper().setAccessToken(response.body().getData().getAccessToken().getAccessToken());
                            }
                            return Single.just(response.body());
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }
}

