package com.taurus.auto.repository;

import android.util.Log;

import com.taurus.auto.data.model.request.ChangeActiveRequest;
import com.taurus.auto.data.model.request.ChangeRoleRequest;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.Groups;
import com.taurus.auto.data.model.response.userlist.DataUserList;
import com.taurus.auto.data.network.ApiServiceTaurusAuto;
import com.taurus.auto.data.prefs.PreferenceHelper;
import com.taurus.auto.utils.AppConstants;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class ClientsRepositoryImp extends BaseRepositoryImp implements ClientsRepository {

    @Inject
    ClientsRepositoryImp(ApiServiceTaurusAuto apiServiceTaurusAuto, PreferenceHelper preferenceHelper) {
        super(apiServiceTaurusAuto, preferenceHelper);
    }

    @Override
    public Single<DataUserList> getClientsList(String search, boolean active, String page) {
        return getApiServiceAuto().getUserList(getPreferenceHelper().getAccessToken(), search, active, 15, page)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<DataProfile> getUserDate(String userId) {
        return getApiServiceAuto().getUserDate(getPreferenceHelper().getAccessToken(), userId)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getDataProfile());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> changeActiveUser(ChangeActiveRequest changeActiveRequest, String userId) {
        return getApiServiceAuto().changeActiveUser(getPreferenceHelper().getAccessToken(), userId, changeActiveRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just("ok");
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> changeRole(ChangeRoleRequest changeRoleRequest, String userId) {
        return getApiServiceAuto().changeRole(getPreferenceHelper().getAccessToken(), userId, changeRoleRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just("ok");
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<List<Groups>> getRole() {
        return getApiServiceAuto().getReferences()
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData().getGroupsList());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }
}
