package com.taurus.auto.repository;

import com.taurus.auto.data.model.request.ChangePasswordRequest;
import com.taurus.auto.data.model.request.ChangeSettingRequest;
import com.taurus.auto.data.model.request.ChangeTimeZoneRequest;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.data.model.request.UpdateProfileDateRequest;
import com.taurus.auto.data.model.response.changepassword.ChangePasswordResponse;
import com.taurus.auto.data.model.response.mainmenu.ItemsMenu;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.TimeZones;
import com.taurus.auto.data.network.ApiServiceTaurusAuto;
import com.taurus.auto.data.prefs.PreferenceHelper;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class ProfileRepositoryImp extends BaseRepositoryImp implements ProfileRepository {

    @Inject
    ProfileRepositoryImp(ApiServiceTaurusAuto apiServiceTaurusAuto, PreferenceHelper preferenceHelper) {
        super(apiServiceTaurusAuto, preferenceHelper);
    }

    @Override
    public Single<List<ItemsMenu>> getMainMenuInfo() {
        return getApiServiceAuto().getMainMenuInfo(getPreferenceHelper().getAccessToken())
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData().getItemsMenuList());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<ChangePasswordResponse> changePassword(ChangePasswordRequest changePasswordRequest) {
        return getApiServiceAuto().changePassword(getPreferenceHelper().getAccessToken(), changePasswordRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            return Single.just(response.body());
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<DataProfile> getProfileInfo() {
        return getApiServiceAuto().getProfileInfo(getPreferenceHelper().getAccessToken())
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getDataProfile());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> changeSetting(ChangeSettingRequest changeSettingRequest) {
        return getApiServiceAuto().changeSetting(getPreferenceHelper().getAccessToken(), changeSettingRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just("ok");
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<List<TimeZones>> getTimezones() {
        return getApiServiceAuto().getReferences()
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData().getTimeZonesList());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> changeTimeZone(ChangeTimeZoneRequest changeTimeZoneRequest) {
        return getApiServiceAuto().changeTimeZone(getPreferenceHelper().getAccessToken(), changeTimeZoneRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData().getMessage());
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> updateDate(UpdateProfileDateRequest updateProfileDateRequest) {
        return getApiServiceAuto().updateDate(getPreferenceHelper().getAccessToken(), updateProfileDateRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                return Single.just(response.body().getData().getMessage());
                            } else if (response.body().getResult().equals("error")) {
                                if (response.body().getErrorsList().get(0).getCode().equals("constraints.phone_confirmed_change_error")) {
                                    return Single.error(new Throwable("phone"));
                                }
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }

    @Override
    public Single<String> updatePhone(RegisterPhoneRequest registerPhoneRequest) {
        return getApiServiceAuto().updatePhone(getPreferenceHelper().getAccessToken(), registerPhoneRequest)
                .flatMap(response -> {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getResult().equals("ok")) {
                                if(response.body().getData().getStatus().getMessage() != null) {
                                    return Single.just(response.body().getData().getStatus().getMessage());
                                } else {
                                    return Single.just("ok");
                                }
                            } else if (response.body().getResult().equals("error")) {
                                return Single.error(new Throwable(response.body().getErrorsList().get(0).getMessage()));
                            }
                        }
                    }
                    return Single.error(new Throwable(response.message()));
                });
    }
}
