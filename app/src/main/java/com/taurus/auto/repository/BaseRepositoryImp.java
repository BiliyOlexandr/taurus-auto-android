package com.taurus.auto.repository;

import com.taurus.auto.data.network.ApiServiceTaurusAuto;
import com.taurus.auto.data.prefs.PreferenceHelper;

import javax.inject.Inject;

public class BaseRepositoryImp implements BaseRepository {

    private ApiServiceTaurusAuto apiServiceTaurusAuto;
    private PreferenceHelper preferenceHelper;

    @Inject
    BaseRepositoryImp(ApiServiceTaurusAuto apiServiceTaurusAuto, PreferenceHelper preferenceHelper){
        this.apiServiceTaurusAuto = apiServiceTaurusAuto;
        this.preferenceHelper = preferenceHelper;
    }

    @Override
    public ApiServiceTaurusAuto getApiServiceAuto() {
        return apiServiceTaurusAuto;
    }

    @Override
    public PreferenceHelper getPreferenceHelper() {
        return preferenceHelper;
    }
}
