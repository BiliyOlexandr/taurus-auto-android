package com.taurus.auto.ui.dialogs;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taurus.auto.R;
import com.taurus.auto.data.model.TimeZoneModel;
import com.taurus.auto.data.model.response.reference.TimeZones;
import com.taurus.auto.ui.adapters.TimeZoneAdapter;
import com.taurus.auto.ui.interfaces.OnClickTimeZone;
import com.taurus.auto.utils.AppConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import static android.app.Activity.RESULT_OK;

public class DialogTimeZone extends DialogFragment implements OnClickTimeZone {

    private RecyclerView listTimeZone;
    private TextView btnSave;
    private LinearLayoutManager linearLayoutManager;
    private TimeZoneAdapter timeZoneAdapter;
    private List<TimeZones> timeZonesList;
    private int idSelect;

    public static DialogTimeZone newInstance(List<TimeZones> timeZonesList, int idSelect) {
        
        Bundle args = new Bundle();
        args.putParcelableArrayList("timeZones", (ArrayList<? extends Parcelable>) timeZonesList);
        args.putInt("idSelect", idSelect);
        
        DialogTimeZone fragment = new DialogTimeZone();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null){
            timeZonesList = bundle.getParcelableArrayList("timeZones");
            idSelect = bundle.getInt("idSelect");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.dialog_select_timezone, container, false);

        listTimeZone = (RecyclerView) v.findViewById(R.id.listTimezone);

        timeZoneAdapter = new TimeZoneAdapter(timeZonesList, idSelect);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        listTimeZone.setLayoutManager(linearLayoutManager);
        listTimeZone.setHasFixedSize(true);
        listTimeZone.setAdapter(timeZoneAdapter);
        timeZoneAdapter.setOnClickTimeZone(this);
        if(idSelect < 3){
            listTimeZone.scrollToPosition(0);
        } else {
            listTimeZone.scrollToPosition(idSelect - 3);
        }
        return v;
    }


    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(AppConstants.TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(AppConstants.TAG, "Dialog 1: onCancel");
    }

    @Override
    public void OnClickTimeZone(TimeZones timezone, int idSelect) {
        Intent intent = new Intent();
        intent.putExtra("timeZone", timezone);
        intent.putExtra("idSelect", idSelect);
        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
        dismiss();
    }
}