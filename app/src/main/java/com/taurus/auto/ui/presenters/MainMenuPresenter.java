package com.taurus.auto.ui.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.repository.ProfileRepository;
import com.taurus.auto.ui.view.MainMenuView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class MainMenuPresenter extends MvpPresenter<MainMenuView> {

    @Inject
    ProfileRepository profileRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public MainMenuPresenter() {
        getActivityComponent().inject(this);
    }

    public void getInfoMainMenu(){
        compositeDisposable.add(profileRepository.getMainMenuInfo()
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().updateAdapter(result);
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}
