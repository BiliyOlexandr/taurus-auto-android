package com.taurus.auto.ui.view;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(SkipStrategy.class)
public interface EditProfileView extends BaseView {

    void openSmsFragment(String phone, String message);

    void closeFragment();

    void showErrorPhone(int message);

    void emailValid();

    void showErrorEmail(int message);

    void updatePhone(String phone);
}
