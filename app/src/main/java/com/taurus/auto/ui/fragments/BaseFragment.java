package com.taurus.auto.ui.fragments;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.taurus.auto.di.ActivityContext;
import com.taurus.auto.di.ApplicationContext;
import com.taurus.auto.di.component.ActivityComponent;
import com.taurus.auto.ui.activities.AuthActivity;
import com.taurus.auto.ui.activities.BaseActivity;
import com.taurus.auto.ui.view.BaseView;

import javax.inject.Inject;

import butterknife.Unbinder;

public class BaseFragment extends MvpAppCompatFragment implements BaseView {

    @Inject
    @ApplicationContext
    Context context;

    private Unbinder mUnBinder;
    private BaseActivity baseActivity;

    public ActivityComponent getActivityComponent() {
        return BaseActivity.getActivityComponent();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        baseActivity = (BaseActivity) getActivity();
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showToast(int message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showActionBar(boolean isShow) {
        if (baseActivity.getSupportActionBar() != null) {
            if (isShow) {
                baseActivity.getSupportActionBar().show();
            } else {
                baseActivity.getSupportActionBar().hide();
            }
        }
    }
}