package com.taurus.auto.ui.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.taurus.auto.ui.activities.ProfileActivity;

public class ChatsFragment extends BaseFragment {

    private ProfileActivity profileActivity;

    public static ChatsFragment newInstance() {
        
        Bundle args = new Bundle();
        
        ChatsFragment fragment = new ChatsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        profileActivity = (ProfileActivity) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        profileActivity.showBottomBar(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        profileActivity.showBottomBar(true);
    }
}
