package com.taurus.auto.ui.interfaces;

import com.taurus.auto.data.model.response.reference.TimeZones;

public interface OnClickTimeZone {

    void OnClickTimeZone(TimeZones timezone, int idSelect);
}
