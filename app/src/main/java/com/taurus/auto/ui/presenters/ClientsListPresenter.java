package com.taurus.auto.ui.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.repository.ClientsRepository;
import com.taurus.auto.ui.view.ClientsListView;
import com.taurus.auto.utils.AppConstants;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class ClientsListPresenter extends MvpPresenter<ClientsListView> {

    private String nextPage = null;

    @Inject
    ClientsRepository clientsRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public ClientsListPresenter() {
        getActivityComponent().inject(this);
    }

    public void getUserList(String query, boolean active, String page) {
        compositeDisposable.add(clientsRepository.getClientsList(query, active, page)
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if(result.getPaging() != null){
                        nextPage = result.getPaging().getNext();
                    }
                    getViewState().showList(result.getUserList(), nextPage == null);

                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    public String getNextPage() {
        return nextPage;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}

