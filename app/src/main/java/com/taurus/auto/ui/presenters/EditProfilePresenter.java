package com.taurus.auto.ui.presenters;

import android.util.Log;
import android.util.Patterns;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.data.model.request.UpdateProfileDateRequest;
import com.taurus.auto.repository.ProfileRepository;
import com.taurus.auto.ui.view.EditProfileView;
import com.taurus.auto.utils.AppConstants;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class EditProfilePresenter extends MvpPresenter<EditProfileView> {

    private boolean isErrorPhone;
    private String finalPhone;

    @Inject
    ProfileRepository profileRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public EditProfilePresenter() {
        getActivityComponent().inject(this);
    }

    public void updateDate(String fullName, String phone, String email) {
        if(phone != null) {
            finalPhone = phone;
            phone = phone.replace(" ", "");
        }
        getViewState().showLoading();
        compositeDisposable.add(profileRepository.updateDate(new UpdateProfileDateRequest(fullName, phone, email))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().hideLoading();
                    if (isErrorPhone) {
                        getViewState().updatePhone(finalPhone);
                    } else {
                        getViewState().closeFragment();
                        getViewState().showToast(result);
                    }
                }, throwable -> {
                    if (throwable.getMessage() != null) {
                        if (throwable.getMessage().equals("phone")) {
                            if (fullName != null || email != null) {
                                isErrorPhone = true;
                                updateDate(fullName, null, email);
                            } else {
                                getViewState().updatePhone(finalPhone);
                            }
                        } else {
                            if (isErrorPhone) {
                                getViewState().updatePhone(finalPhone);
                            }
                            getViewState().showToast(throwable.getMessage());
                        }
                    }
                    getViewState().hideLoading();
                })
        );
    }

    public void updatePhone(String phone) {
        String finalPhone = phone;
        phone = phone.replace(" ", "");
        getViewState().showLoading();
        compositeDisposable.add(profileRepository.updatePhone(new RegisterPhoneRequest(phone, null))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().hideLoading();
                    getViewState().openSmsFragment(finalPhone, result);
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                    getViewState().hideLoading();
                })
        );
    }

    public void isValidEmail(CharSequence charSequence) {
        boolean isValid = Patterns.EMAIL_ADDRESS.matcher(charSequence).matches();
        if (isValid) getViewState().emailValid();
        else getViewState().showErrorEmail(R.string.email_invalide);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}
