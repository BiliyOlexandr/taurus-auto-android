package com.taurus.auto.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.response.mainmenu.ItemsMenu;
import com.taurus.auto.ui.activities.ProfileActivity;
import com.taurus.auto.ui.adapters.MainMenuAdapter;
import com.taurus.auto.ui.presenters.MainMenuPresenter;
import com.taurus.auto.ui.view.MainMenuView;
import com.taurus.auto.utils.AppConstants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainMenuFragment extends BaseFragment implements MainMenuView {

    @Inject
    MainMenuAdapter mainMenuAdapter;

    @InjectPresenter
    MainMenuPresenter mainMenuPresenter;

    @BindView(R.id.listMainMenu)
    RecyclerView listMainMenu;

    private ProfileActivity profileActivity;

    public static MainMenuFragment newInstance() {
        
        Bundle args = new Bundle();

        MainMenuFragment fragment = new MainMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        profileActivity = (ProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_meny, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(false);

        listMainMenu.setLayoutManager(new GridLayoutManager(profileActivity, 2));
        listMainMenu.setHasFixedSize(true);
        listMainMenu.setAdapter(mainMenuAdapter);

        mainMenuPresenter.getInfoMainMenu();

        return view;
    }

    @Override
    public void updateAdapter(List<ItemsMenu> itemsMenuList) {
        mainMenuAdapter.addItems(itemsMenuList);
    }

    @Override
    public void onStart() {
        super.onStart();
        profileActivity.showBottomBar(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        profileActivity.showBottomBar(true);
    }
}
