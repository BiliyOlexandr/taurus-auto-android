package com.taurus.auto.ui.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.data.model.request.ChangePasswordRequest;
import com.taurus.auto.repository.ProfileRepository;
import com.taurus.auto.ui.view.ChangePasswordView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class ChangePasswordPresenter extends MvpPresenter<ChangePasswordView> {

    @Inject
    ProfileRepository profileRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public ChangePasswordPresenter() {
        getActivityComponent().inject(this);
    }

    public void changePassword(String currentPassword, String newPassword) {
        compositeDisposable.add(profileRepository.changePassword(new ChangePasswordRequest(currentPassword, newPassword))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.getResult().equals("ok")) {
                        getViewState().showToast(result.getData().getMessage());
                        getViewState().closeFragment();
                    } else if (result.getResult().equals("error")) {
                        if (result.getErrorsList().get(0).getField().equals("current_password")) {
                            getViewState().errorPassword(1, result.getErrorsList().get(0).getMessage());
                        } else {
                            getViewState().errorPassword(2, result.getErrorsList().get(0).getMessage());
                        }
                    }

                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}

