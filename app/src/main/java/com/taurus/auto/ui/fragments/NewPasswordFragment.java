package com.taurus.auto.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.ui.activities.ProfileActivity;
import com.taurus.auto.ui.presenters.NewPasswordPresenter;
import com.taurus.auto.ui.view.NewPasswordView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewPasswordFragment extends BaseFragment implements NewPasswordView {

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.includeErrorPassword)
    View includeErrorPassword;

    @BindView(R.id.editNewPassword)
    EditText editNewPassword;

    @BindView(R.id.imgHidePassword)
    ImageView imgHidePassword;

    @BindView(R.id.imgWatchPassword)
    ImageView imgWatchPassword;

    @InjectPresenter
    NewPasswordPresenter newPasswordPresenter;

    private TextView txError;
    private boolean visiblePassword;
    private String phone, code, password;

    public static NewPasswordFragment newInstance(String phone, String code) {

        Bundle args = new Bundle();
        args.putString("phone", phone);
        args.putString("code", code);

        NewPasswordFragment fragment = new NewPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null){
            phone = bundle.getString("phone", phone);
            code = bundle.getString("code", code);
        }
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_password, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        txError = (TextView) includeErrorPassword.findViewById(R.id.txError);

        imgHidePassword.setOnClickListener(v -> {
            editNewPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editNewPassword.setSelection(editNewPassword.getText().length());
            visiblePassword = false;
            imgHidePassword.setVisibility(View.GONE);
            imgWatchPassword.setVisibility(View.VISIBLE);
        });

        imgWatchPassword.setOnClickListener(v -> {
            editNewPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            editNewPassword.setSelection(editNewPassword.getText().length());
            visiblePassword = true;
            imgWatchPassword.setVisibility(View.GONE);
            imgHidePassword.setVisibility(View.VISIBLE);
        });

        editNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0){
                    if(visiblePassword){
                        imgHidePassword.setVisibility(View.VISIBLE);
                    } else {
                        imgWatchPassword.setVisibility(View.VISIBLE);
                    }
                } else {
                    imgHidePassword.setVisibility(View.GONE);
                    imgWatchPassword.setVisibility(View.GONE);
                }
                includeErrorPassword.setVisibility(View.GONE);
            }
        });

        btnSave.setOnClickListener(v -> newPasswordPresenter.isValidPassword(editNewPassword.getText().toString()));
        return view;
    }

    @Override
    public void openProfile() {
        Intent intent = new Intent(getBaseActivity(), ProfileActivity.class);
        getBaseActivity().startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void showError(int error) {
        includeErrorPassword.setVisibility(View.VISIBLE);
        txError.setText(getBaseActivity().getResources().getString(error));
    }

    @Override
    public void showError(String error) {
        includeErrorPassword.setVisibility(View.VISIBLE);
        txError.setText(error);
    }

    @Override
    public void isValidPassword(String password) {
        newPasswordPresenter.resetPasswordNew(phone, code, password);
    }
}
