package com.taurus.auto.ui.view;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.Groups;

import java.util.List;

@StateStrategyType(SkipStrategy.class)
public interface ClientInfoView extends BaseView {

    void showDate(DataProfile dataProfile);

    void changeActive(boolean isActive);

    void setListRole(List<Groups> rolesList, int idSelectRole);

    void setRoleOld(String titleRoleOld, int RoleOld);
}
