package com.taurus.auto.ui.adapters;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taurus.auto.R;
import com.taurus.auto.data.model.TimeZoneModel;
import com.taurus.auto.data.model.response.reference.TimeZones;
import com.taurus.auto.ui.interfaces.OnClickTimeZone;
import com.taurus.auto.utils.AppConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeZoneAdapter extends RecyclerView.Adapter<TimeZoneAdapter.TimeZoneHolder> {

    private List<TimeZones> timezoneList;
    private OnClickTimeZone onClickTimeZone;
    private int idSelect;

    public class TimeZoneHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txTimezone)
        TextView txTimeZone;

        TimeZoneHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> onClickTimeZone.OnClickTimeZone(timezoneList.get(getAdapterPosition()), getAdapterPosition()));
        }
    }

    public TimeZoneAdapter(List<TimeZones> timezoneList, int idSelect) {
        this.timezoneList = timezoneList;
        this.idSelect = idSelect;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public TimeZoneHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_timezone, viewGroup, false);
        return new TimeZoneHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final TimeZoneHolder timeZoneHolder, final int position) {
        timeZoneHolder.txTimeZone.setText(timezoneList.get(position).getTitle());
        if (idSelect == position){
            timeZoneHolder.txTimeZone.setTextColor(Color.parseColor("#6093fd"));
        } else {
            timeZoneHolder.txTimeZone.setTextColor(Color.parseColor("#2d343d"));
        }
    }

    @Override
    public int getItemCount() {
        return timezoneList.size();
    }

    public void setOnClickTimeZone(OnClickTimeZone onClickTimeZone) {
        this.onClickTimeZone = onClickTimeZone;
    }

    private void setIdSelect(int idSelect){
        this.idSelect = idSelect;
    }
}
