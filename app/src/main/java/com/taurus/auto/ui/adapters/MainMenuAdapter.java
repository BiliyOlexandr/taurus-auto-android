package com.taurus.auto.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taurus.auto.R;
import com.taurus.auto.data.enums.Menu;
import com.taurus.auto.data.model.response.mainmenu.ItemsMenu;
import com.taurus.auto.ui.activities.ClientsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.MainMenuHolder> {

    private List<ItemsMenu> mainMenuList;
    private Context context;

    public class MainMenuHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txNameMenu)
        TextView txNamMenu;

        @BindView(R.id.imgFonMenu)
        ImageView imgFonMenu;

        @BindView(R.id.imgMenu)
        ImageView imgMenu;

        MainMenuHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                Intent intent = null;
                if (mainMenuList.get(getAdapterPosition()).getName().equals(Menu.USERS.getValue())) {
                    intent = new Intent(context, ClientsActivity.class);
                }
                if(intent != null) context.startActivity(intent);
            });
        }
    }

    public MainMenuAdapter(List<ItemsMenu> mainMenuList, Context context) {
        this.mainMenuList = mainMenuList;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public MainMenuHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_menu, viewGroup, false);
        return new MainMenuHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MainMenuHolder mainMenuHolder, final int position) {
        String title = mainMenuList.get(position).getTitle();
        if (mainMenuList.get(position).getName().equals(Menu.CHAT.getValue())) {
            setValue(mainMenuHolder, title, Menu.CHAT.getIdImage(), Menu.CHAT.getIdFon());
        } else if (mainMenuList.get(position).getName().equals(Menu.CHATS.getValue())) {
            setValue(mainMenuHolder, title, Menu.CHATS.getIdImage(), Menu.CHATS.getIdFon());
        } else if (mainMenuList.get(position).getName().equals(Menu.FAQ.getValue())) {
            setValue(mainMenuHolder, title, Menu.FAQ.getIdImage(), Menu.FAQ.getIdFon());
        } else if (mainMenuList.get(position).getName().equals(Menu.PLATFORMS.getValue())) {
            setValue(mainMenuHolder, title, Menu.PLATFORMS.getIdImage(), Menu.PLATFORMS.getIdFon());
        } else if (mainMenuList.get(position).getName().equals(Menu.RECOMMEND.getValue())) {
            setValue(mainMenuHolder, title, Menu.RECOMMEND.getIdImage(), Menu.RECOMMEND.getIdFon());
        } else if (mainMenuList.get(position).getName().equals(Menu.TEXTS.getValue())) {
            setValue(mainMenuHolder, title, Menu.TEXTS.getIdImage(), Menu.TEXTS.getIdFon());
        } else if (mainMenuList.get(position).getName().equals(Menu.USERS.getValue())) {
            setValue(mainMenuHolder, title, Menu.USERS.getIdImage(), Menu.USERS.getIdFon());
        }
    }

    @Override
    public int getItemCount() {
        return mainMenuList.size();
    }

    public void addItems(List<ItemsMenu> mainMenuList) {
        this.mainMenuList.addAll(mainMenuList);
        notifyDataSetChanged();
    }

    private void setValue(MainMenuHolder mainMenuHolder, String text, int imgMenu, int imgFon) {
        mainMenuHolder.txNamMenu.setText(text);
        mainMenuHolder.imgFonMenu.setImageResource(imgFon);
        mainMenuHolder.imgMenu.setImageResource(imgMenu);
    }
}
