package com.taurus.auto.ui.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.data.model.request.ChangeActiveRequest;
import com.taurus.auto.data.model.request.ChangeRoleRequest;
import com.taurus.auto.data.model.response.reference.Groups;
import com.taurus.auto.repository.ClientsRepository;
import com.taurus.auto.ui.view.ClientInfoView;
import com.taurus.auto.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class ClientInfoPresenter extends MvpPresenter<ClientInfoView> {

    @Inject
    ClientsRepository clientsRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public ClientInfoPresenter() {
        getActivityComponent().inject(this);
    }

    public void getUserDate(String idUser) {
        Log.d(AppConstants.TAG, "21312");
        compositeDisposable.add(clientsRepository.getUserDate(idUser)
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().showDate(result);
                    getViewState().hideLoading();
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    public void getRoles(String idSelectRole) {
        compositeDisposable.add(clientsRepository.getRole()
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().setListRole(result, getIdSelect(idSelectRole, result));
                    getViewState().hideLoading();
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    public void changeActive(boolean isActive, String userId) {
        compositeDisposable.add(clientsRepository.changeActiveUser(new ChangeActiveRequest(isActive), userId)
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                    getViewState().changeActive(!isActive);
                })
        );
    }

    public void changeRole(String nameRole, String titleRoleOld, int idSelectRoleOld, String userId) {
        List<String> roleList = new ArrayList<>();
        roleList.add(nameRole);
        compositeDisposable.add(clientsRepository.changeRole(new ChangeRoleRequest(roleList), userId)
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    Log.d(AppConstants.TAG, "good");
                }, throwable -> {
                    Log.d(AppConstants.TAG, "faild");
                    getViewState().setRoleOld(titleRoleOld, idSelectRoleOld);
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }


    private int getIdSelect(String idSelectRole, List<Groups> rolesList){
        for (int i = 0; i < rolesList.size(); i++){
            if(rolesList.get(i).getName().equals(idSelectRole)){
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}

