package com.taurus.auto.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.Groups;
import com.taurus.auto.ui.activities.RoleActivity;
import com.taurus.auto.ui.presenters.ClientInfoPresenter;
import com.taurus.auto.ui.view.ClientInfoView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

public class ClientsInfoFragment extends BaseFragment implements ClientInfoView {

    public final static int REQUEST_ROLE = 1;
    private String idUser;
    private boolean isChange = false;
    private List<Groups> rolesList = new ArrayList<>();
    private int idSelectRole;

    @InjectPresenter
    ClientInfoPresenter clientInfoPresenter;

    @BindView(R.id.txAvatar)
    TextView txAvatar;

    @BindView(R.id.editName)
    EditText editName;

    @BindView(R.id.editLastName)
    EditText editLastName;

    @BindView(R.id.editPhone)
    EditText editPhone;

    @BindView(R.id.editEmail)
    EditText editEmail;

    @BindView(R.id.layoutRole)
    ConstraintLayout layoutRole;

    @BindView(R.id.switchDeactive)
    Switch switchDeactive;

    @BindView(R.id.tx_role_name)
    TextView txRole;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.layoutContent)
    NestedScrollView layoutContent;

    public static ClientsInfoFragment newInstance(String idUser) {
        Bundle args = new Bundle();
        args.putString("idUser", idUser);

        ClientsInfoFragment fragment = new ClientsInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            idUser = bundle.getString("idUser");
        }

        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_info, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        clientInfoPresenter.getUserDate(idUser);

        switchDeactive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChange) {
                clientInfoPresenter.changeActive(!isChecked, idUser);
            } else {
                isChange = true;
            }
        });

        layoutRole.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), RoleActivity.class);
            intent.putParcelableArrayListExtra("roles", (ArrayList<? extends Parcelable>) rolesList);
            intent.putExtra("idSelect", idSelectRole);
            startActivityForResult(intent, REQUEST_ROLE);
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.edit_profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showDate(DataProfile dataProfile) {
        clientInfoPresenter.getRoles(dataProfile.getUser().getGroupList().get(0).getName());
        editName.setText(dataProfile.getUser().getFirstName());
        if (dataProfile.getUser().getLastName() == null) {
            txAvatar.setText(String.valueOf(dataProfile.getUser().getFirstName().charAt(0)).toUpperCase());
        } else {
            txAvatar.setText(String.valueOf(dataProfile.getUser().getFirstName().charAt(0)).toUpperCase() +
                    String.valueOf(dataProfile.getUser().getLastName().charAt(0)).toUpperCase());
            editLastName.setText(dataProfile.getUser().getLastName());
        }
        editPhone.setText(dataProfile.getUserData().getPhoneNumber().getPhoneNumber());
        if (dataProfile.getUserData().getEmail() != null && dataProfile.getUserData().getEmail().getEmail() != null) {
            editEmail.setText(dataProfile.getUserData().getEmail().getEmail());
        }
        txRole.setText(dataProfile.getUser().getGroupList().get(0).getTitle());

        if (!dataProfile.getUser().getStatusUser().isActive()) {
            switchDeactive.setChecked(true);
        } else {
            isChange = true;
        }
    }

    @Override
    public void changeActive(boolean isActive) {
        isChange = false;
        switchDeactive.setChecked(!isActive);
    }

    @Override
    public void setListRole(List<Groups> rolesList, int idSelectRole) {
        this.rolesList = rolesList;
        this.idSelectRole = idSelectRole;
    }

    @Override
    public void setRoleOld(String titleRoleOld, int idRoleOld) {
        txRole.setText(titleRoleOld);
        idSelectRole = idRoleOld;
    }

    @Override
    public void showLoading() {
        super.showLoading();
        progressBar.setVisibility(View.VISIBLE);
        layoutContent.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        setHasOptionsMenu(true);
        progressBar.setVisibility(View.GONE);
        layoutContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ROLE) {
                if (data != null) {
                    Groups role = data.getExtras().getParcelable("role");
                    clientInfoPresenter.changeRole(role.getName(), txRole.getText().toString(), idSelectRole, idUser);
                    txRole.setText(role.getTitle());
                    idSelectRole = data.getExtras().getInt("idSelect");
                }
            }
        }
    }
}
