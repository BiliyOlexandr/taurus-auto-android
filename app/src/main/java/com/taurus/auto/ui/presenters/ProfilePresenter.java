package com.taurus.auto.ui.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.data.model.request.ChangeSettingRequest;
import com.taurus.auto.data.model.request.ChangeTimeZoneRequest;
import com.taurus.auto.data.model.response.reference.TimeZones;
import com.taurus.auto.repository.ProfileRepository;
import com.taurus.auto.ui.view.ProfileView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class ProfilePresenter extends MvpPresenter<ProfileView> {

    @Inject
    ProfileRepository profileRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public ProfilePresenter() {
        getActivityComponent().inject(this);
    }

    public void getProfileInfo() {
        compositeDisposable.add(profileRepository.getProfileInfo()
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().visibleDateProfile(result);
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    public void changeSetting(boolean isNotificationPush, boolean isEmailPush) {
        compositeDisposable.add(profileRepository.changeSetting(new ChangeSettingRequest(isNotificationPush, isEmailPush))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                    getViewState().changeSetting(!isNotificationPush);
                })
        );
    }

    public void getTimeZones(int idSelectZone) {
        getViewState().showLoading();
        compositeDisposable.add(profileRepository.getTimezones()
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().setListTimeZones(result, getIdSelect(idSelectZone, result));
                    getViewState().hideLoading();
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                    getViewState().hideLoading();
                })
        );
    }

    public void changeTimeZone(int zoneId, String timeZonesOld, int idSelectOld) {
        compositeDisposable.add(profileRepository.changeTimeZone(new ChangeTimeZoneRequest(zoneId))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                }, throwable -> {
                    getViewState().setTimeZonesOld(timeZonesOld, idSelectOld);
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }

    private int getIdSelect(int idSelectZone, List<TimeZones> timeZonesList){
        for (int i = 0; i < timeZonesList.size(); i++){
            if(timeZonesList.get(i).getZoneId() == idSelectZone){
                return i;
            }
        }
        return 0;
    }
}