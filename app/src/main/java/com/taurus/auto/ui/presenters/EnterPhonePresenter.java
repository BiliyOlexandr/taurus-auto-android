package com.taurus.auto.ui.presenters;

import android.content.Intent;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.repository.AuthTaurusRepository;
import com.taurus.auto.ui.view.EnterPhoneView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class EnterPhonePresenter extends MvpPresenter<EnterPhoneView> {

    @Inject
    AuthTaurusRepository authTaurusRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public EnterPhonePresenter() {
        getActivityComponent().inject(this);
    }

    public void registerPhone(String phone) {
        phone = phone.replace(" ", "");
        getViewState().showLoading();
        compositeDisposable.add(authTaurusRepository.registerPhone(new RegisterPhoneRequest(phone, null))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().hideLoading();
                    getViewState().openSmsCodeFragment(result);
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                    getViewState().hideLoading();
                })
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}
