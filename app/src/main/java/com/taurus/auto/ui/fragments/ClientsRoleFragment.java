package com.taurus.auto.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taurus.auto.R;
import com.taurus.auto.data.model.response.reference.Groups;
import com.taurus.auto.ui.activities.ClientsActivity;
import com.taurus.auto.ui.adapters.RolesAdapter;
import com.taurus.auto.ui.interfaces.OnClickRoles;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientsRoleFragment extends BaseFragment implements OnClickRoles {

    private List<Groups> rolesList = new ArrayList<>();
    private int idSelectRole;
    private RolesAdapter rolesAdapter;

    @BindView(R.id.listRoles)
    RecyclerView recyclerView;

    public static ClientsRoleFragment newInstance(Bundle args) {

        ClientsRoleFragment fragment = new ClientsRoleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null){
            rolesList = bundle.getParcelableArrayList("roles");
            idSelectRole = bundle.getInt("idSelect");
        }
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_role, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        rolesAdapter = new RolesAdapter(rolesList, idSelectRole);
        rolesAdapter.setOnClickRoles(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(rolesAdapter);

        return view;
    }

    @Override
    public void obClickRole(Groups groups, int idSelect) {
        Intent intent = new Intent();
        intent.putExtra("role", rolesList.get(idSelect));
        intent.putExtra("idSelect", idSelect);
        requireActivity().setResult(Activity.RESULT_OK, intent);
        requireActivity().finish();
    }
}
