package com.taurus.auto.ui.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.data.model.request.LoginRequest;
import com.taurus.auto.repository.AuthTaurusRepository;
import com.taurus.auto.ui.view.SignInView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class SignInPresenter extends MvpPresenter<SignInView> {

    @Inject
    AuthTaurusRepository authTaurusRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public SignInPresenter(){getActivityComponent().inject(this);}

    public void login(String username, String password) {
        getViewState().showLoading();
        compositeDisposable.add(authTaurusRepository.login(new LoginRequest(username, password))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().openProfile();
                }, throwable -> {
                    if(throwable.getMessage() != null && throwable.getMessage().equals("password")){
                        getViewState().showErrorPasswordOrLogin(2);
                    } else {
                        getViewState().showErrorPasswordOrLogin(1);
                    }
                    getViewState().hideLoading();
                })
        );
    }
}
