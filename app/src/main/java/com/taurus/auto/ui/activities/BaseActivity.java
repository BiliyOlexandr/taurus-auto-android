package com.taurus.auto.ui.activities;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.taurus.auto.MvpApp;
import com.taurus.auto.di.component.ActivityComponent;
import com.taurus.auto.di.component.DaggerActivityComponent;
import com.taurus.auto.di.module.ActivityModule;
import com.taurus.auto.utils.AppConstants;

import butterknife.Unbinder;

public class BaseActivity extends AppCompatActivity {

    private Unbinder mUnBinder;
    private static ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .appComponent(((MvpApp) getApplication()).getAppComponent())
                .build();
    }

    public static ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        Log.d(AppConstants.TAG, "333");
        super.onDestroy();
    }
}
