package com.taurus.auto.ui.activities;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.taurus.auto.R;
import com.taurus.auto.ui.fragments.ChangePasswordFragment;
import com.taurus.auto.ui.fragments.EditProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfileActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        setUnBinder(ButterKnife.bind(this, this));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_backicon);

        if(getIntent().getExtras().getInt("idFragment") == 1) {
            openEditProfileFragment();
        } else {
            openChangeFragment();
        }
    }

    private void openChangeFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, ChangePasswordFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();
    }

    private void openEditProfileFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, EditProfileFragment.newInstance(getIntent().getExtras().getParcelable("dataProfile")));
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            checkFragmentStach();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            checkFragmentStach();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void checkFragmentStach() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
