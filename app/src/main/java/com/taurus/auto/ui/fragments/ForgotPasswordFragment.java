package com.taurus.auto.ui.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.ui.presenters.ForgotPasswordPresenter;
import com.taurus.auto.ui.view.EnterPhoneView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgotPasswordFragment extends BaseFragment implements EnterPhoneView {

    @BindView(R.id.imgClear)
    ImageView imgClear;

    @BindView(R.id.btnSend)
    Button btnSend;

    @BindView(R.id.editForgotPassword)
    EditText editForgotPassword;

    @BindView(R.id.inclideErrorPhone)
    View includeErrorPhone;

    @InjectPresenter
    ForgotPasswordPresenter forgotPasswordPresenter;

    private TextView txError;

    public static ForgotPasswordFragment newInstance() {

        Bundle args = new Bundle();

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        txError = (TextView) includeErrorPhone.findViewById(R.id.txError);

        editForgotPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                if (editForgotPassword.getText().toString().isEmpty()) {
                    setEditText();
                }
                imgClear.setVisibility(View.VISIBLE);
            } else {
                imgClear.setVisibility(View.GONE);
            }
        });

        btnSend.setOnClickListener(v -> {
            forgotPasswordPresenter.resetPasswordPhone(editForgotPassword.getText().toString());
        });

        imgClear.setOnClickListener(v -> {
            editForgotPassword.setText("");
        });

        editForgotPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                includeErrorPhone.setVisibility(View.GONE);
            }
        });

        return view;
    }

    private void setEditText() {
        editForgotPassword.setText("+380");
        editForgotPassword.setSelection(editForgotPassword.getText().length());
    }

    @Override
    public void showLoading() {
        super.showLoading();
        btnSend.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        btnSend.setEnabled(true);
    }

    @Override
    public void openSmsCodeFragment(String message) {
        FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, SmsCodeFragment.newInstance(editForgotPassword.getText().toString(), 2, message));
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void showErrorPhone(int message) {
        includeErrorPhone.setVisibility(View.VISIBLE);
        txError.setText(getBaseActivity().getResources().getString(message));
    }
}
