package com.taurus.auto.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.ui.activities.EditProfileActivity;
import com.taurus.auto.ui.activities.ProfileActivity;
import com.taurus.auto.ui.presenters.ChangePasswordPresenter;
import com.taurus.auto.ui.view.ChangePasswordView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordFragment extends BaseFragment implements ChangePasswordView {

    private EditProfileActivity editProfileActivity;

    @InjectPresenter
    ChangePasswordPresenter changePasswordPresenter;

    @BindView(R.id.editNewPassword)
    EditText editNewPassword;

    @BindView(R.id.editOldPassword)
    EditText editOldPassword;

    @BindView(R.id.imgCheckNewPassword)
    ImageView imgCheckNewPassword;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.includeErrorNewPassword)
    View includeErrorNewPassword;

    @BindView(R.id.includeErrorOldPassword)
    View includeErrorOldPassword;

    private TextView txErrorNewPassword;
    private TextView txErrorOldPassword;

    public static ChangePasswordFragment newInstance() {

        Bundle args = new Bundle();
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        editProfileActivity = (EditProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        txErrorNewPassword = (TextView) includeErrorNewPassword.findViewById(R.id.txError);
        txErrorOldPassword = (TextView) includeErrorOldPassword.findViewById(R.id.txError);

        editOldPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                includeErrorOldPassword.setVisibility(View.GONE);
            }
        });

        editNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() >= 8){
                    imgCheckNewPassword.setVisibility(View.VISIBLE);
                } else {
                    imgCheckNewPassword.setVisibility(View.GONE);
                }
                includeErrorNewPassword.setVisibility(View.GONE);
            }
        });

        btnSave.setOnClickListener(v -> {
            if (editOldPassword.getText().toString().isEmpty()){
                includeErrorOldPassword.setVisibility(View.VISIBLE);
                txErrorOldPassword.setText(editProfileActivity.getResources().getString(R.string.empty_field));
            } else if (editNewPassword.getText().toString().isEmpty()){
                includeErrorNewPassword.setVisibility(View.VISIBLE);
                txErrorNewPassword.setText(editProfileActivity.getResources().getString(R.string.empty_field));
            } else {
                changePasswordPresenter.changePassword(editOldPassword.getText().toString(), editNewPassword.getText().toString());
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        showActionBar(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        showActionBar(false);
    }

    @Override
    public void closeFragment() {
        editProfileActivity.finish();
    }

    @Override
    public void errorPassword(int idField, String message) {
        if(idField == 1){
            includeErrorOldPassword.setVisibility(View.VISIBLE);
            txErrorOldPassword.setText(message);
        } else {
            includeErrorNewPassword.setVisibility(View.GONE);
            txErrorNewPassword.setText(message);
        }
    }
}
