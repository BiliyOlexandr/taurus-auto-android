package com.taurus.auto.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.taurus.auto.R;
import com.taurus.auto.data.model.response.userlist.User;
import com.taurus.auto.ui.activities.BaseActivity;
import com.taurus.auto.ui.fragments.ClientsFragment;
import com.taurus.auto.ui.fragments.ClientsInfoFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.ClientsHolder> {

    private List<User> userList;
    private Context context;

    public class ClientsHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txAvatar)
        TextView txAvatar;

        @BindView(R.id.txName)
        TextView txName;

        @BindView(R.id.txPhone)
        TextView txPhone;

        ClientsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                FragmentTransaction ft = ((BaseActivity)context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_frame_holder, ClientsInfoFragment.newInstance(userList.get(getAdapterPosition()).getId()));
                ft.addToBackStack(null);
                ft.commit();
            });
        }
    }

    public ClientsAdapter(List<User> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ClientsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_client, viewGroup, false);
        return new ClientsHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ClientsHolder clientsHolder, final int position) {
        if (userList.get(position).getLastName() == null) {
            clientsHolder.txAvatar.setText(String.valueOf(userList.get(position).getFirstName().charAt(0)).toUpperCase());
            clientsHolder.txName.setText(String.valueOf(userList.get(position).getFirstName()));
        } else {
            clientsHolder.txAvatar.setText(String.valueOf(userList.get(position).getFirstName().charAt(0)).toUpperCase() +
                    String.valueOf(userList.get(position).getLastName().charAt(0)).toUpperCase());
            clientsHolder.txName.setText(userList.get(position).getFirstName() + " " + userList.get(position).getLastName());
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void addItems(List<User> userList){
        this.userList.addAll(userList);
        notifyDataSetChanged();
    }
}
