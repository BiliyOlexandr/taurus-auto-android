package com.taurus.auto.ui.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.data.model.request.RegisterPhoneRequest;
import com.taurus.auto.data.model.request.ResetPasswordRequest;
import com.taurus.auto.repository.AuthTaurusRepository;
import com.taurus.auto.repository.ProfileRepository;
import com.taurus.auto.ui.view.SmsCodeView;
import com.taurus.auto.utils.AppConstants;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class SmsCodePresenter extends MvpPresenter<SmsCodeView> {

    @Inject
    CompositeDisposable compositeDisposable;

    @Inject
    AuthTaurusRepository authTaurusRepository;

    @Inject
    ProfileRepository profileRepository;

    public SmsCodePresenter() {
        getActivityComponent().inject(this);
    }


    public void onReceiveSMS(String sms) {
        String pattern = ".*<#>.*([c07/F1jWN8R])";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(sms);
        if (m.find()) {
            Matcher matcher = Pattern.compile("\\d+").matcher(m.group(0));
            if (matcher.find()) {
                if (matcher.group(0) != null && matcher.group(0).length() == 6) {
                    getViewState().fillField(matcher.group(0));
                }
            } else {
                Log.d(AppConstants.TAG, "no-match");
            }
        } else {
            Log.d(AppConstants.TAG, "no-match");
        }
    }

    public void isValidCode(String phone, String code) {
        phone = phone.replace(" ", "");
        getViewState().showLoading();
        compositeDisposable.add(authTaurusRepository.checkPhone(new RegisterPhoneRequest(phone, code))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().openProfile();
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    public void isValidCodeUpdate(String phone, String code) {
        phone = phone.replace(" ", "");
        getViewState().showLoading();
        compositeDisposable.add(profileRepository.updatePhone(new RegisterPhoneRequest(phone, code))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().showToast(result);
                    getViewState().closeFragment();
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    public void isValidCodeResetPassword(String phone, String code) {
        phone = phone.replace(" ", "");
        String phoneFinal = phone;
        getViewState().showLoading();
        compositeDisposable.add(authTaurusRepository.resetPasswordCode(new ResetPasswordRequest(phone, code, null))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().openNewPassword(phoneFinal, code);
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                })
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}
