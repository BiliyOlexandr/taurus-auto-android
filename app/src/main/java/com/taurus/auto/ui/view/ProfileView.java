package com.taurus.auto.ui.view;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.TimeZones;

import java.util.List;

@StateStrategyType(SkipStrategy.class)
public interface ProfileView extends BaseView {

    void visibleDateProfile(DataProfile dataProfile);

    void changeSetting(boolean isNotice);

    void openDialogTimeZones(List<TimeZones> timeZonesList);

    void setTimeZonesOld(String timeZonesOld, int idSelectOld);

    void setListTimeZones(List<TimeZones> timeZonesList, int idSelectTimeZone);
}
