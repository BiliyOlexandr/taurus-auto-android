package com.taurus.auto.ui.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.request.ResetPasswordRequest;
import com.taurus.auto.repository.AuthTaurusRepository;
import com.taurus.auto.ui.view.NewPasswordView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

@InjectViewState
public class NewPasswordPresenter extends MvpPresenter<NewPasswordView> {

    @Inject
    AuthTaurusRepository authTaurusRepository;

    @Inject
    CompositeDisposable compositeDisposable;

    public NewPasswordPresenter() {
        getActivityComponent().inject(this);
    }

    public void resetPasswordNew(String phone, String code, String password) {
        getViewState().showLoading();
        compositeDisposable.add(authTaurusRepository.resetPasswordNew(new ResetPasswordRequest(phone, code, password))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.getResult().equals("ok")) {
                        getViewState().openProfile();
                    } else if (result.getResult().equals("error")) {
                        getViewState().hideLoading();
                        getViewState().showError(result.getErrorsList().get(0).getMessage());
                    }
                }, throwable -> {
                    getViewState().showToast(throwable.getMessage());
                    getViewState().hideLoading();
                })
        );
    }

    public void isValidPassword(String password) {
        if(password.isEmpty()){
            getViewState().showError(R.string.empty_field);
        } else {
            getViewState().isValidPassword(password);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}
