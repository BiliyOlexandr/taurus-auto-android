package com.taurus.auto.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.taurus.auto.data.model.response.listchat.Chats;
import com.taurus.auto.databinding.ItemListChatAddBinding;
import com.taurus.auto.databinding.ItemListChatBinding;

import java.util.ArrayList;
import java.util.List;

public class ListChatAdapter extends ListAdapter<Chats, ListChatAdapter.ViewHolder> {

    private static final int TYPE_CHAT = 0;
    private static final int TYPE_ADD_CHAT = 1;

    protected ListChatAdapter() {
        super(new DiffUtil.ItemCallback<Chats>() {
            @Override
            public boolean areItemsTheSame(@NonNull Chats oldItem, @NonNull Chats newItem) {
                return oldItem.getChatId().equals(newItem.getChatId());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Chats oldItem, @NonNull Chats newItem) {
                return oldItem.equals(newItem);
            }
        });
    }

    @Override
    public void submitList(@Nullable List<Chats> list) {
        ArrayList<Chats> newList = new ArrayList<>();
        if (list != null) {
            newList.addAll(list);
            newList.add(null);
        }
        super.submitList(newList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_CHAT) {
            return new ListChatsViewHolder(ItemListChatBinding.inflate(inflater, parent, false));
        } else {
            return new AddListChatHolder(ItemListChatAddBinding.inflate(inflater, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == getItemCount()) {
            return TYPE_ADD_CHAT;
        } else {
            return TYPE_CHAT;
        }
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        abstract void bind(Chats model);
    }

    class ListChatsViewHolder extends ViewHolder {
        ListChatsViewHolder(@NonNull ItemListChatBinding itemView) {
            super(itemView.getRoot());
        }

        @Override
        void bind(Chats model) {

        }
    }

    class AddListChatHolder extends ViewHolder {

        AddListChatHolder(@NonNull ItemListChatAddBinding itemView) {
            super(itemView.getRoot());
        }

        @Override
        void bind(Chats model) {

        }
    }
}
