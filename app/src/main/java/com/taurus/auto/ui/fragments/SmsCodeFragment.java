package com.taurus.auto.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.taurus.auto.R;
import com.taurus.auto.receiver.SMSReceiver;
import com.taurus.auto.ui.activities.EditProfileActivity;
import com.taurus.auto.ui.activities.ProfileActivity;
import com.taurus.auto.ui.presenters.SmsCodePresenter;
import com.taurus.auto.ui.view.SmsCodeView;
import com.taurus.auto.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SmsCodeFragment extends BaseFragment implements SmsCodeView, SMSReceiver.OTPReceiveListener {

    private String phone, message;
    private boolean isEditOneFull, isEditTwoFull, isEditThreeFull, isEditFourFull, isEditFiveFull, isEditSixFull;
    private int idFragment;

    @BindView(R.id.editCodeOne)
    EditText editCodeOne;

    @BindView(R.id.editCodeTwo)
    EditText editCodeTwo;

    @BindView(R.id.editCodeThree)
    EditText editCodeThree;

    @BindView(R.id.editCodeFour)
    EditText editCodeFour;

    @BindView(R.id.editCodeFive)
    EditText editCodeFive;

    @BindView(R.id.editCodeSix)
    EditText editCodeSix;

    @BindView(R.id.txSmsCodeInfo)
    TextView txSmsCodeInfo;

    @BindView(R.id.txReturnCode)
    TextView txReturnCode;

    private SMSReceiver smsReceiver;
    private EditProfileActivity editProfileActivity;

    @InjectPresenter
    SmsCodePresenter smsCodePresenter;

    public static SmsCodeFragment newInstance(String phone, int idFragment, String message) {
        Bundle args = new Bundle();
        args.putString("phone", phone);
        args.putInt("idFragment", idFragment);
        args.putString("message", message);
        SmsCodeFragment fragment = new SmsCodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof EditProfileActivity) {
            editProfileActivity = (EditProfileActivity) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            phone = bundle.getString("phone");
            idFragment = bundle.getInt("idFragment");
            message = bundle.getString("message");
        }
        getActivityComponent().inject(this);
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter_sms, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        txSmsCodeInfo.setText(message);
        startSMSListener();

        txReturnCode.setOnClickListener(v -> getBaseActivity().getSupportFragmentManager().popBackStack());

        editCodeOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    editCodeTwo.requestFocus();
                    isEditOneFull = true;
                } else {
                    isEditOneFull = false;
                }
                isAllEditFull();
            }
        });
        editCodeTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    editCodeThree.requestFocus();
                    isEditTwoFull = true;
                } else {
                    isEditTwoFull = false;
                }
                isAllEditFull();
            }
        });
        editCodeThree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    editCodeFour.requestFocus();
                    isEditThreeFull = true;
                } else {
                    isEditThreeFull = false;
                }
                isAllEditFull();
            }
        });
        editCodeFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    editCodeFive.requestFocus();
                    isEditFourFull = true;
                } else {
                    isEditFourFull = false;
                }
                isAllEditFull();
            }
        });
        editCodeFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    editCodeSix.requestFocus();
                    isEditFiveFull = true;
                } else {
                    isEditFiveFull = false;
                }
                isAllEditFull();
            }
        });
        editCodeSix.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                isEditSixFull = s.length() == 1;
                isAllEditFull();
            }
        });

        editCodeTwo.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (editCodeTwo.getText().length() == 0) {
                    editCodeOne.requestFocus();
                    editCodeOne.setText("");
                    return true;
                }
            }
            return false;
        });

        editCodeThree.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (editCodeThree.getText().length() == 0) {
                    editCodeTwo.requestFocus();
                    editCodeTwo.setText("");
                    return true;
                }
            }
            return false;
        });

        editCodeFour.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (editCodeFour.getText().length() == 0) {
                    editCodeThree.requestFocus();
                    editCodeThree.setText("");
                    return true;
                }
            }
            return false;
        });

        editCodeFive.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (editCodeFive.getText().length() == 0) {
                    editCodeFour.requestFocus();
                    editCodeFour.setText("");
                }
            }
            return false;
        });

        editCodeSix.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (editCodeSix.getText().length() == 0) {
                    editCodeFive.requestFocus();
                    editCodeFive.setText("");
                }
            }
            return false;
        });

        return view;
    }

    private void startSMSListener() {
        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            getBaseActivity().registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(getBaseActivity());

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(aVoid -> {
                // API successfully started
            });

            task.addOnFailureListener(e -> {
                // Fail to start API
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOTPReceived(String otp) {
        Log.d(AppConstants.TAG, otp);
        smsCodePresenter.onReceiveSMS(otp);
        if (smsReceiver != null) {
            getBaseActivity().unregisterReceiver(smsReceiver);
            smsReceiver = null;
        }
    }

    @Override
    public void onOTPTimeOut() {
        Log.d(AppConstants.TAG, "OTP Time out");
    }

    @Override
    public void onOTPReceivedError(String error) {
        Log.d(AppConstants.TAG, error);
    }

    @Override
    public void fillField(String code) {
        if (idFragment == 1) {
            smsCodePresenter.isValidCode(phone, code);
        } else if (idFragment == 2) {
            smsCodePresenter.isValidCodeResetPassword(phone, code);
        } else {
            smsCodePresenter.isValidCodeUpdate(phone, code);
        }
    }

    @Override
    public void openProfile() {
        Intent intent = new Intent(getBaseActivity(), ProfileActivity.class);
        getBaseActivity().startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void openNewPassword(String phone, String code) {
        getBaseActivity().getSupportFragmentManager().popBackStack();
        FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, NewPasswordFragment.newInstance(phone, code));
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void closeFragment() {
        editProfileActivity.finish();
    }

    private void isAllEditFull() {
        if (isEditOneFull && isEditTwoFull && isEditThreeFull && isEditFourFull && isEditFiveFull && isEditSixFull) {
            String smsCode = editCodeOne.getText().toString() + editCodeTwo.getText().toString() + editCodeThree.getText().toString()
                    + editCodeFour.getText().toString() + editCodeFive.getText().toString() + editCodeSix.getText().toString();
            if (idFragment == 1) {
                smsCodePresenter.isValidCode(phone, smsCode);
            } else if (idFragment == 2) {
                smsCodePresenter.isValidCodeResetPassword(phone, smsCode);
            } else {
                smsCodePresenter.isValidCodeUpdate(phone, smsCode);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (editProfileActivity != null) {
            showActionBar(false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        showActionBar(true);

    }
}
