package com.taurus.auto.ui.view;

import androidx.annotation.StringRes;

import com.arellomobile.mvp.MvpView;

public interface BaseView extends MvpView {

    void showToast(String message);
    void showToast(@StringRes int message);
    void showLoading();
    void hideLoading();
    void showActionBar(boolean isShow);
}
