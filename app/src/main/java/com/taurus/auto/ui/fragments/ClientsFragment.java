package com.taurus.auto.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.taurus.auto.R;
import com.taurus.auto.ui.adapters.ViewPagerTabAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientsFragment extends BaseFragment {

    @BindView(R.id.tab_clients)
    TabLayout tabLayout;

    @BindView(R.id.pager_clients)
    ViewPager viewPager;

    private ViewPagerTabAdapter viewPagerTabAdapter;

    public static ClientsFragment newInstance() {
        Bundle args = new Bundle();

        ClientsFragment fragment = new ClientsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clients, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.edit_profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setupViewPager(ViewPager viewPager) {

        viewPagerTabAdapter = new ViewPagerTabAdapter(getChildFragmentManager());

        if (getActivity() != null) {
            viewPagerTabAdapter.addFrag(ClientListFragment.newInstance(1), getActivity().getResources().getString(R.string.client_tab_active));
            viewPagerTabAdapter.addFrag(ClientListFragment.newInstance(2), getActivity().getResources().getString(R.string.client_tab_not_active));
            viewPager.setAdapter(viewPagerTabAdapter);
        }
    }
}
