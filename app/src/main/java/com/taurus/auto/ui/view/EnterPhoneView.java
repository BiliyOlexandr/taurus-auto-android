package com.taurus.auto.ui.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(SkipStrategy.class)
public interface EnterPhoneView extends BaseView {

    void openSmsCodeFragment(String message);

    void showErrorPhone(int message);
}
