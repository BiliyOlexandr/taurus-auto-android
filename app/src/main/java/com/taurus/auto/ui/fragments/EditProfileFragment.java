package com.taurus.auto.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.ui.activities.EditProfileActivity;
import com.taurus.auto.ui.activities.ProfileActivity;
import com.taurus.auto.ui.presenters.EditProfilePresenter;
import com.taurus.auto.ui.view.EditProfileView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfileFragment extends BaseFragment implements EditProfileView {

    private EditProfileActivity editProfileActivity;
    private DataProfile dataProfile;
    private TextView txErrorPhone, txErrorEmail, txInfoEmail, txMenuEnable, txMenu;
    private MenuItem itemSave;
    private boolean isChangeColor, isChange;

    @InjectPresenter
    EditProfilePresenter editProfilePresenter;

    @BindView(R.id.editPhone)
    EditText editPhone;

    @BindView(R.id.editEmail)
    EditText editEmail;

    @BindView(R.id.editName)
    EditText editName;

    @BindView(R.id.txMatch)
    TextView txMatch;

    @BindView(R.id.imgCheckEmail)
    ImageView imgCheckEmail;

    @BindView(R.id.imgCheckPhone)
    ImageView imgCheckPhone;

    @BindView(R.id.includeErrorPhoneProfile)
    View includeErrorPhone;

    @BindView(R.id.includeErrorEmailProfile)
    View includeErrorEmailProfile;

    @BindView(R.id.layout_info_email)
    View includeInfoEmail;

    @BindView(R.id.imgInfo)
    ImageView imgInfo;

    @BindView(R.id.imgInfoActive)
    ImageView imgInfoActive;

    public static EditProfileFragment newInstance(DataProfile dataProfile) {

        Bundle args = new Bundle();
        args.putParcelable("dataProfile", dataProfile);

        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        editProfileActivity = (EditProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            dataProfile = bundle.getParcelable("dataProfile");
        }
        setHasOptionsMenu(true);
        getActivityComponent().inject(this);
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        txErrorPhone = (TextView) includeErrorPhone.findViewById(R.id.txError);
        txErrorEmail = (TextView) includeErrorEmailProfile.findViewById(R.id.txError);
        txInfoEmail = (TextView) includeInfoEmail.findViewById(R.id.txTooltip);

        if (dataProfile.getUser().getLastName() == null) {
            editName.setText(dataProfile.getUser().getFirstName());
        } else {
            editName.setText(dataProfile.getUser().getFirstName() + " " + dataProfile.getUser().getLastName());
        }

        editPhone.setText(dataProfile.getUserData().getPhoneNumber().getPhoneNumber());

        if (dataProfile.getUserData().getEmail() != null && dataProfile.getUserData().getEmail().getEmail() != null) {
            editEmail.setText(dataProfile.getUserData().getEmail().getEmail());
            if (dataProfile.getUserData().getEmail().isConfirmed()) {
                imgCheckEmail.setVisibility(View.VISIBLE);
                imgInfo.setVisibility(View.GONE);
            }
        }

        if (dataProfile.getUserData().getPhoneNumber().isConfirmed()) {
            imgCheckPhone.setVisibility(View.VISIBLE);
        }

        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                includeErrorPhone.setVisibility(View.GONE);
                changeColorMenu();
            }
        });

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                changeColorMenu();
            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                changeColorMenu();
                includeErrorEmailProfile.setVisibility(View.GONE);
                if (imgCheckEmail.getVisibility() != View.VISIBLE) {
                    imgInfo.setVisibility(View.VISIBLE);
                    imgInfoActive.setVisibility(View.GONE);
                }
                includeInfoEmail.setVisibility(View.GONE);
            }
        });

        imgInfo.setOnClickListener(v -> {
            imgInfo.setVisibility(View.GONE);
            imgInfoActive.setVisibility(View.VISIBLE);
            includeInfoEmail.setVisibility(View.VISIBLE);
        });

        imgInfoActive.setOnClickListener(v -> {
            imgInfo.setVisibility(View.VISIBLE);
            imgInfoActive.setVisibility(View.GONE);
            includeInfoEmail.setVisibility(View.GONE);
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.edit_profile_menu, menu);
        itemSave = menu.findItem(R.id.menu_save);
        itemSave.setActionView(R.layout.item_menu_save);
        txMenuEnable = itemSave.getActionView().findViewById(R.id.menu_save_enable);
        txMenu = itemSave.getActionView().findViewById(R.id.menu_save_unenable);
        txMenuEnable.setOnClickListener(v -> {
            String fullName = null;
            if (!editName.getText().toString().isEmpty()) fullName = editName.getText().toString();
            if (!editEmail.getText().toString().isEmpty()) {
                editProfilePresenter.isValidEmail(editEmail.getText().toString());
            } else {
                editProfilePresenter.updateDate(fullName, editPhone.getText().toString(), null);
            }
        });
        if (isChange) {
            txMenuEnable.setVisibility(View.VISIBLE);
            txMenu.setVisibility(View.GONE);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showLoading() {
        super.showLoading();
        txMenuEnable.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        txMenuEnable.setEnabled(true);
    }

    @Override
    public void openSmsFragment(String phone, String message) {
        isChange = true;
        FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, SmsCodeFragment.newInstance(phone, 3, message));
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void closeFragment() {
        editProfileActivity.finish();
    }

    @Override
    public void showErrorPhone(int message) {
        includeErrorPhone.setVisibility(View.VISIBLE);
        txErrorPhone.setText(getBaseActivity().getResources().getString(message));
    }

    @Override
    public void emailValid() {
        String fullName = null;
        if (!editName.getText().toString().isEmpty()) fullName = editName.getText().toString();
        editProfilePresenter.updateDate(fullName, editPhone.getText().toString(), editEmail.getText().toString());
    }

    @Override
    public void showErrorEmail(int message) {
        includeErrorEmailProfile.setVisibility(View.VISIBLE);
        txErrorEmail.setText(getBaseActivity().getResources().getString(message));
    }

    @Override
    public void updatePhone(String phone) {
        editProfilePresenter.updatePhone(phone);
    }

    private void changeColorMenu() {
        if (!isChangeColor) {
            txMenuEnable.setVisibility(View.VISIBLE);
            txMenu.setVisibility(View.GONE);
            isChangeColor = true;
        }
    }
}
