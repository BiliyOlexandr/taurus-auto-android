package com.taurus.auto.ui.view;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(SkipStrategy.class)
public interface SmsCodeView extends BaseView {

    void fillField(String code);

    void openProfile();

    void openNewPassword(String phone, String code);

    void closeFragment();
}
