package com.taurus.auto.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.response.profileinfo.DataProfile;
import com.taurus.auto.data.model.response.reference.TimeZones;
import com.taurus.auto.ui.activities.EditProfileActivity;
import com.taurus.auto.ui.activities.ProfileActivity;
import com.taurus.auto.ui.dialogs.DialogTimeZone;
import com.taurus.auto.ui.dialogs.ExitDialog;
import com.taurus.auto.ui.presenters.ProfilePresenter;
import com.taurus.auto.ui.view.ProfileView;
import com.taurus.auto.utils.AppConstants;

import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment implements ProfileView {

    private static final int REQUEST_TIMEZONE = 1;
    private ProfileActivity profileActivity;
    private DataProfile dataProfile;
    private boolean isChange = false;
    private List<TimeZones> timeZonesList;
    private int idSelectTimeZone;

    @InjectPresenter
    ProfilePresenter profilePresenter;

    @BindView(R.id.tx_exit)
    TextView txExit;

    @BindView(R.id.layoutChangePassword)
    ConstraintLayout layoutChangePassword;

    @BindView(R.id.switchNotification)
    Switch switchNotification;

    @BindView(R.id.layoutTimezone)
    ConstraintLayout layoutTimezone;

    @BindView(R.id.txTimezone)
    TextView txTimezone;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.constraintLayout2)
    ConstraintLayout layoutFullContent;

    @BindView(R.id.txName)
    TextView txName;

    @BindView(R.id.txLastName)
    TextView txLastName;

    @BindView(R.id.txAvatar)
    TextView txAvatarName;

    @BindView(R.id.txPhone)
    TextView txPhone;

    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        profileActivity = (ProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(true);

        switchNotification.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChange) {
                profilePresenter.changeSetting(isChecked, dataProfile.getUserData().getNoticeSetting().isEmailNoticeEnabled());
            } else {
                isChange = true;
            }
        });

        txExit.setOnClickListener(v -> {
            DialogFragment exitDialog = ExitDialog.newInstance();
            if (getFragmentManager() != null) {
                exitDialog.show(getFragmentManager(), "exitDialog");
            }
        });

        layoutTimezone.setOnClickListener(v -> {
            openDialogTimeZones(timeZonesList);
        });

        layoutChangePassword.setOnClickListener(v -> {
            Intent intent = new Intent(profileActivity, EditProfileActivity.class);
            intent.putExtra("idFragment", 2);
            profileActivity.startActivity(intent);
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.edit_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_edit) {
            Intent intent = new Intent(profileActivity, EditProfileActivity.class);
            intent.putExtra("dataProfile", dataProfile);
            intent.putExtra("idFragment", 1);
            profileActivity.startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TIMEZONE) {
                if (data != null) {
                    TimeZones timeZones = data.getExtras().getParcelable("timeZone");
                    profilePresenter.changeTimeZone(timeZones.getZoneId(), txTimezone.getText().toString(), idSelectTimeZone);
                    txTimezone.setText(timeZones.getTitle());
                    idSelectTimeZone = data.getExtras().getInt("idSelect");
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        profileActivity.showBottomBar(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        profilePresenter.getProfileInfo();
        profileActivity.showBottomBar(true);
        hideKeyboard();
    }

    private void hideKeyboard() {
        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    @Override
    public void showLoading() {
        super.showLoading();
        progressBar.setVisibility(View.VISIBLE);
        layoutFullContent.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        setHasOptionsMenu(true);
        progressBar.setVisibility(View.GONE);
        layoutFullContent.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void visibleDateProfile(DataProfile dataProfile) {
        profilePresenter.getTimeZones(dataProfile.getUserData().getTimeZones().getZoneId());
        this.dataProfile = dataProfile;
        if (dataProfile.getUser().getLastName() == null) {
            txName.setText(dataProfile.getUser().getFirstName());
            txLastName.setVisibility(View.GONE);
            txAvatarName.setText(String.valueOf(dataProfile.getUser().getFirstName().charAt(0)).toUpperCase());
        } else {
            txName.setText(dataProfile.getUser().getFirstName());
            txLastName.setVisibility(View.VISIBLE);
            txLastName.setText(dataProfile.getUser().getLastName());
            txAvatarName.setText(String.valueOf(dataProfile.getUser().getFirstName().charAt(0)).toUpperCase() +
                    String.valueOf(dataProfile.getUser().getLastName().charAt(0)).toUpperCase());
        }

        txPhone.setText(dataProfile.getUserData().getPhoneNumber().getPhoneNumber());

        if (dataProfile.getUserData().getNoticeSetting().isPushNoticeEnabled()) {
            switchNotification.setChecked(true);
        } else {
            isChange = true;
        }

        txTimezone.setText(dataProfile.getUserData().getTimeZones().getTitle());
    }

    @Override
    public void changeSetting(boolean isNotice) {
        isChange = false;
        switchNotification.setChecked(isNotice);
    }

    @Override
    public void openDialogTimeZones(List<TimeZones> timeZonesList) {
        DialogFragment dialogFragment = DialogTimeZone.newInstance(timeZonesList, idSelectTimeZone);
        dialogFragment.setTargetFragment(this, REQUEST_TIMEZONE);
        if (getFragmentManager() != null) {
            dialogFragment.show(getFragmentManager(), "timeZoneDialog");
        }
    }

    @Override
    public void setTimeZonesOld(String timeZonesOld, int idSelectOld) {
        txTimezone.setText(timeZonesOld);
        idSelectTimeZone = idSelectOld;
    }

    @Override
    public void setListTimeZones(List<TimeZones> timeZonesList, int idSelectTimeZone) {
        this.timeZonesList = timeZonesList;
        this.idSelectTimeZone = idSelectTimeZone;
    }
}
