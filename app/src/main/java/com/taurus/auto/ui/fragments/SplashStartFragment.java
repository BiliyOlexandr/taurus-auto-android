package com.taurus.auto.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.taurus.auto.R;
import com.taurus.auto.di.ActivityContext;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashStartFragment extends BaseFragment {

    @BindView(R.id.btn_next)
    Button btnNext;

    @Inject
    @ActivityContext
    Context context;

    public static SplashStartFragment newInstance() {

        Bundle args = new Bundle();

        SplashStartFragment fragment = new SplashStartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_splash_start, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(false);

        btnNext.setOnClickListener(v -> {
            FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_frame_holder, EnterPhoneFragment.newInstance());
            ft.addToBackStack(null);
            ft.commit();
        });
        return view;
    }
}
