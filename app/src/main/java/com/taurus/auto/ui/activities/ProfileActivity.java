package com.taurus.auto.ui.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.taurus.auto.R;
import com.taurus.auto.ui.fragments.ChatsFragment;
import com.taurus.auto.ui.fragments.MainMenuFragment;
import com.taurus.auto.ui.fragments.ProfileFragment;
import com.taurus.auto.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.bottomNavigationView)
    BottomNavigationView bottomBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setUnBinder(ButterKnife.bind(this, this));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new MainMenuFragment();
        ft.replace(R.id.fragment_frame_profile, fragment);
        ft.addToBackStack(null);
        ft.commit();

        bottomBar.setOnNavigationItemSelectedListener(item -> {
            Fragment fragment1 = new MainMenuFragment();

            switch (item.getItemId()) {
                case R.id.bottom_navigation_car:
                    fragment1 = new MainMenuFragment();
                    break;
                case R.id.bottom_navigation_chats:
                    fragment1 = new ChatsFragment();
                    break;
                case R.id.bottom_navigation_profile:
                    fragment1 = new ProfileFragment();
                    break;
            }

            FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
            ft1.replace(R.id.fragment_frame_profile, fragment1);
            ft1.addToBackStack(null);
            ft1.commit();
            return true;
        });
    }

    public void showBottomBar(boolean isShow) {
        if (isShow) {
            bottomBar.setVisibility(View.VISIBLE);
        } else {
            bottomBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}