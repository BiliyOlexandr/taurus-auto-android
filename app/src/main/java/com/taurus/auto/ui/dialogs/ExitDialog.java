package com.taurus.auto.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.taurus.auto.R;
import com.taurus.auto.data.prefs.PreferenceHelper;
import com.taurus.auto.ui.activities.AuthActivity;

import javax.inject.Inject;

import static com.taurus.auto.ui.activities.BaseActivity.getActivityComponent;

public class ExitDialog extends DialogFragment {

    @Inject
    PreferenceHelper preferenceHelper;

    public static ExitDialog newInstance() {
        Bundle args = new Bundle();
        ExitDialog fragment = new ExitDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getResources().getString(R.string.exit_dialog_title))
                .setPositiveButton(getActivity().getResources().getString(R.string.exit_positive_button),
                        (dialog, which) -> {
                            preferenceHelper.setAccessToken(null);
                            Intent intent = new Intent(getActivity(), AuthActivity.class);
                            intent.putExtra("idFragment", 1);
                            getActivity().startActivity(intent);
                            getActivity().finish();
                        })
                .setNegativeButton(getActivity().getResources().getString(R.string.exit_negative_button), null)
                .show();
    }
}
