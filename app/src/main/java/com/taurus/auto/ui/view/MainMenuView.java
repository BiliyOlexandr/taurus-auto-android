package com.taurus.auto.ui.view;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.taurus.auto.data.model.response.mainmenu.ItemsMenu;

import java.util.List;

@StateStrategyType(SkipStrategy.class)
public interface MainMenuView extends BaseView {

    void updateAdapter(List<ItemsMenu> itemsMenuList);
}
