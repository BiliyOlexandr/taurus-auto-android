package com.taurus.auto.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.di.ActivityContext;
import com.taurus.auto.ui.presenters.EnterPhonePresenter;
import com.taurus.auto.ui.view.EnterPhoneView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

public class EnterPhoneFragment extends BaseFragment implements EnterPhoneView {

    private final String TAG = EnterPhoneFragment.class.getName();

    @Inject
    @ActivityContext
    Context context;

    @InjectPresenter
    EnterPhonePresenter enterPhonePresenter;

    @BindView(R.id.edit_enter_phone)
    EditText editEnterPhone;

    @BindView(R.id.btn_get_code)
    Button btnGetCode;

    @BindView(R.id.imgClear)
    ImageView imageClear;

    @BindView(R.id.tx_sign_in)
    TextView txSignIn;

    @BindView(R.id.inclideErrorPhone)
    View includeErrorPhone;

    private TextView txErrorPhone;

    public static EnterPhoneFragment newInstance() {

        Bundle args = new Bundle();

        EnterPhoneFragment fragment = new EnterPhoneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter_phone, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(false);
        txErrorPhone = (TextView) includeErrorPhone.findViewById(R.id.txError);

        editEnterPhone.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                if (editEnterPhone.getText().toString().isEmpty()) {
                    setEditText();
                }
                imageClear.setVisibility(View.VISIBLE);
            } else {
                imageClear.setVisibility(View.GONE);
            }
        });

        btnGetCode.setOnClickListener(v -> {
            enterPhonePresenter.registerPhone(editEnterPhone.getText().toString());
        });

        imageClear.setOnClickListener(v -> {
            editEnterPhone.setText("");
        });

        txSignIn.setOnClickListener(v -> {
            FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_frame_holder, SignInFragment.newInstance());
            ft.addToBackStack(null);
            ft.commit();
        });

        editEnterPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                includeErrorPhone.setVisibility(View.GONE);
            }
        });
        return view;
    }

    @Override
    public void showLoading() {
        super.showLoading();
        btnGetCode.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        btnGetCode.setEnabled(true);
    }

    @Override
    public void openSmsCodeFragment(String message) {
        FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, SmsCodeFragment.newInstance(editEnterPhone.getText().toString(), 1, message));
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void showErrorPhone(int message) {
        includeErrorPhone.setVisibility(View.VISIBLE);
        txErrorPhone.setText(getBaseActivity().getResources().getString(message));
    }

    private void setEditText() {
        editEnterPhone.setText("+380");
        editEnterPhone.setSelection(editEnterPhone.getText().length());
    }
}
