package com.taurus.auto.ui.view;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(SkipStrategy.class)
public interface ChangePasswordView extends BaseView {

    void closeFragment();

    void errorPassword(int idField, String message);
}
