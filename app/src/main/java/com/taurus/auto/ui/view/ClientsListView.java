package com.taurus.auto.ui.view;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.taurus.auto.data.model.response.userlist.User;

import java.util.List;

@StateStrategyType(SkipStrategy.class)
public interface ClientsListView extends BaseView {

    void showList(List<User> userList, boolean isLastPage);
}
