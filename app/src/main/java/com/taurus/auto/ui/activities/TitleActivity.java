package com.taurus.auto.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.taurus.auto.data.prefs.PreferenceHelper;

import javax.inject.Inject;


public class TitleActivity extends BaseActivity {

    @Inject
    PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(this);

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            Intent intent;
            if(preferenceHelper.getAccessToken() != null){
                intent = new Intent(TitleActivity.this, ProfileActivity.class);
            } else {
                intent = new Intent(TitleActivity.this, AuthActivity.class);
            }
            startActivity(intent);
        }, 2000);

//        InstanceID myID = InstanceID.getInstance(this);
//        String registrationToken = myID.getToken(
//                getString(R.string.gcm_defaultSenderId),
//                GoogleCloudMessaging.INSTANCE_ID_SCOPE,
//                null
//        );
//        Log.d(AppConstants.TAG, )
    }
}
