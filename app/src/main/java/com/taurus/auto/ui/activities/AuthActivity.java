package com.taurus.auto.ui.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.taurus.auto.R;
import com.taurus.auto.ui.fragments.SignInFragment;
import com.taurus.auto.ui.fragments.SplashStartFragment;
import com.taurus.auto.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        setUnBinder(ButterKnife.bind(this, this));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_backicon);

        int idFragment = 0;
        if(getIntent().getExtras() != null) {
            idFragment = getIntent().getExtras().getInt("idFragment", 0);
        }
        if(idFragment == 1){
            openLoginFragment();
        } else {
            openSplashStartFragment();
        }
    }

    private void openSplashStartFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, SplashStartFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();
    }

    private void openLoginFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, SignInFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            checkFragmentStach();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            checkFragmentStach();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void checkFragmentStach(){
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
