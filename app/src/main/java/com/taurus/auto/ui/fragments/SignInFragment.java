package com.taurus.auto.ui.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.ui.activities.ProfileActivity;
import com.taurus.auto.ui.presenters.SignInPresenter;
import com.taurus.auto.ui.view.SignInView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignInFragment extends BaseFragment implements SignInView {

    @BindView(R.id.editEmailPhone)
    EditText editEmailPhone;

    @BindView(R.id.editPassword)
    EditText editPassword;

    @BindView(R.id.tx_forgot_password)
    TextView txForgotPassword;

    @BindView(R.id.btn_sign_in)
    Button btnSignIn;

    @BindView(R.id.imgClearProfile)
    ImageView imgClearProfile;

    @BindView(R.id.imgClearPassword)
    ImageView imgClearPassword;

    @BindView(R.id.txRegister)
    TextView txRegister;

    @BindView(R.id.includeErrorProfile)
    View includeErrorProfile;

    @BindView(R.id.includeErrorPassword)
    View includeErrorPassword;

    @InjectPresenter
    SignInPresenter signInPresenter;

    private TextView txErrorProfile, txErrorPassword;

    public static SignInFragment newInstance() {

        Bundle args = new Bundle();

        SignInFragment fragment = new SignInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        showActionBar(false);

        txErrorPassword = (TextView) includeErrorPassword.findViewById(R.id.txError);
        txErrorProfile = (TextView) includeErrorProfile.findViewById(R.id.txError);

        editEmailPhone.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus && editEmailPhone.getText().toString().length() > 0) {
                imgClearProfile.setVisibility(View.VISIBLE);
            } else {
                imgClearProfile.setVisibility(View.GONE);
            }
        });

        editPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus && editPassword.getText().toString().length() > 0) {
                imgClearPassword.setVisibility(View.VISIBLE);
            } else {
                imgClearPassword.setVisibility(View.GONE);
            }
        });

        imgClearPassword.setOnClickListener(v -> {
            editPassword.setText("");
        });

        imgClearProfile.setOnClickListener(v -> {
            editEmailPhone.setText("");
        });

        txRegister.setOnClickListener(v -> {
            FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_frame_holder, EnterPhoneFragment.newInstance());
            ft.addToBackStack(null);
            ft.commit();
        });

        txForgotPassword.setOnClickListener(v -> {
            FragmentTransaction ft = getBaseActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_frame_holder, ForgotPasswordFragment.newInstance());
            ft.addToBackStack(null);
            ft.commit();
        });

        btnSignIn.setOnClickListener(v -> {
            if(editPassword.getText().toString().isEmpty()){
                includeErrorPassword.setVisibility(View.VISIBLE);
                txErrorPassword.setText(getBaseActivity().getResources().getString(R.string.empty_field));
            } else if(editEmailPhone.getText().toString().isEmpty()){
                includeErrorProfile.setVisibility(View.VISIBLE);
                txErrorProfile.setText(getBaseActivity().getResources().getString(R.string.empty_field));
            } else {
                signInPresenter.login(editEmailPhone.getText().toString(), editPassword.getText().toString());
            }
        });

        editEmailPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    imgClearProfile.setVisibility(View.VISIBLE);
                } else {
                    imgClearProfile.setVisibility(View.GONE);
                }
                editEmailPhone.setTextColor(Color.parseColor("#2d343d"));
                includeErrorProfile.setVisibility(View.GONE);
            }
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    imgClearPassword.setVisibility(View.VISIBLE);
                } else {
                    imgClearPassword.setVisibility(View.GONE);
                }
                editPassword.setTextColor(Color.parseColor("#2d343d"));
                includeErrorPassword.setVisibility(View.GONE);
            }
        });

        return view;
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        btnSignIn.setEnabled(true);
    }

    @Override
    public void showLoading() {
        super.showLoading();
        btnSignIn.setEnabled(false);
    }

    @Override
    public void openProfile() {
        Intent intent = new Intent(getBaseActivity(), ProfileActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void showErrorPasswordOrLogin(int id) {
        if(id == 2){
            txErrorPassword.setText(R.string.error_password);
            includeErrorPassword.setVisibility(View.VISIBLE);
            editPassword.setTextColor(Color.parseColor("#ff2c2c"));
        } else {
            txErrorProfile.setText(R.string.error_login);
            includeErrorProfile.setVisibility(View.VISIBLE);
            editEmailPhone.setTextColor(Color.parseColor("#ff2c2c"));
        }
    }
}
