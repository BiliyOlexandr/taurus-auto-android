package com.taurus.auto.ui.activities;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.taurus.auto.R;
import com.taurus.auto.ui.fragments.ClientsRoleFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoleActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        setUnBinder(ButterKnife.bind(this, this));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_backicon);

        openClientRoleFragment();
    }

    private void openClientRoleFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, ClientsRoleFragment.newInstance(getIntent().getExtras()));
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            checkFragmentStach();
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkFragmentStach(){
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}

