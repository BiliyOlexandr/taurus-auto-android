package com.taurus.auto.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.taurus.auto.R;
import com.taurus.auto.data.model.response.userlist.User;
import com.taurus.auto.ui.adapters.ClientsAdapter;
import com.taurus.auto.ui.presenters.ClientsListPresenter;
import com.taurus.auto.ui.view.ClientsListView;
import com.taurus.auto.utils.PaginationListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientListFragment extends BaseFragment implements ClientsListView {

    private int idFragment;
    private boolean isLastPage, isLoading;
    private ClientsAdapter adapter;
    private List<User> userList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.list_clients)
    RecyclerView recyclerClients;

    @InjectPresenter
    ClientsListPresenter clientsListPresenter;

    public static ClientListFragment newInstance(int idFragment) {
        Bundle args = new Bundle();
        args.putInt("id", idFragment);

        ClientListFragment fragment = new ClientListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            idFragment = bundle.getInt("id");
        }
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_clients, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        adapter = new ClientsAdapter(userList, requireActivity());
        linearLayoutManager = new LinearLayoutManager(requireActivity());
        recyclerClients.setLayoutManager(linearLayoutManager);
        recyclerClients.setHasFixedSize(true);
        recyclerClients.setAdapter(adapter);

        recyclerClients.addOnScrollListener(new PaginationListener(linearLayoutManager) {

            @Override
            protected void loadMoreItems() {
                isLoading = true;
                clientsListPresenter.getUserList(null, idFragment == 1, clientsListPresenter.getNextPage());
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        return view;
    }

    @Override
    public void showList(List<User> userList, boolean isLastPage) {
        adapter.addItems(userList);
        isLoading = false;
        this.isLastPage = isLastPage;
    }

    @Override
    public void onStop() {
        super.onStop();
        userList.clear();
    }

    @Override
    public void onStart() {
        super.onStart();
        clientsListPresenter.getUserList(null, idFragment == 1, null);
    }
}
