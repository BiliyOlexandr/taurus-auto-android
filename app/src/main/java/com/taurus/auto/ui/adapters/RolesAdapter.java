package com.taurus.auto.ui.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taurus.auto.R;
import com.taurus.auto.data.model.response.reference.Groups;
import com.taurus.auto.ui.interfaces.OnClickRoles;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RolesAdapter extends RecyclerView.Adapter<RolesAdapter.RoleHolder> {

    private List<Groups> rolesList;
    private OnClickRoles onClickRoles;
    private int idSelect;

    public class RoleHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView txName;

        @BindView(R.id.imgCheck)
        ImageView imgCheck;

        RoleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                idSelect = getAdapterPosition();
                onClickRoles.obClickRole(rolesList.get(getAdapterPosition()), getAdapterPosition());
                notifyDataSetChanged();
            });
        }
    }

    public RolesAdapter(List<Groups> rolesList, int idSelect) {
        this.rolesList = rolesList;
        this.idSelect = idSelect;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public RoleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_role, viewGroup, false);
        return new RoleHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RoleHolder roleHolder, final int position) {
        roleHolder.txName.setText(rolesList.get(position).getTitle());
        if (idSelect == position){
            roleHolder.txName.setTextAppearance(R.style.TextView16SFProDisplaySemiBold2d343d);
            roleHolder.imgCheck.setVisibility(View.VISIBLE);
        } else {
            roleHolder.txName.setTextAppearance(R.style.TextView16SFProDisplayRegular2d343d);
            roleHolder.imgCheck.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return rolesList.size();
    }

    public void setOnClickRoles(OnClickRoles onClickRoles) {
        this.onClickRoles = onClickRoles;
    }
}
