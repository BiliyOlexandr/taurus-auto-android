package com.taurus.auto.di.module;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taurus.auto.data.model.response.mainmenu.ItemsMenu;
import com.taurus.auto.di.ActivityContext;
import com.taurus.auto.repository.AuthTaurusRepository;
import com.taurus.auto.repository.AuthTaurusRepositoryImp;
import com.taurus.auto.repository.ChatsRepository;
import com.taurus.auto.repository.ChatsRepositoryImp;
import com.taurus.auto.repository.ClientsRepository;
import com.taurus.auto.repository.ClientsRepositoryImp;
import com.taurus.auto.repository.ProfileRepository;
import com.taurus.auto.repository.ProfileRepositoryImp;
import com.taurus.auto.ui.adapters.MainMenuAdapter;
import com.taurus.auto.ui.presenters.ClientInfoPresenter;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    AppCompatActivity activity;

    public ActivityModule (AppCompatActivity activity){
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext(){
        return activity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }

    @Provides
    AuthTaurusRepository provideAuthTaurusRepository(AuthTaurusRepositoryImp authTaurusRepositoryImp){
        return authTaurusRepositoryImp;
    }

    @Provides
    MainMenuAdapter providerMainMenuAdapter(@ActivityContext Context context){
      return new MainMenuAdapter(new ArrayList<ItemsMenu>(), context);
    }

    @Provides
    ProfileRepository provideProvideRepository(ProfileRepositoryImp profileRepositoryImp){
        return profileRepositoryImp;
    }

    @Provides
    ClientsRepository provideClientsRepository(ClientsRepositoryImp clientsRepositoryImp){
        return clientsRepositoryImp;
    }

    @Provides
    ChatsRepository provideChatsRepository(ChatsRepositoryImp chatsRepositoryImp){
        return chatsRepositoryImp;
    }
}
