package com.taurus.auto.di.module;

import android.app.Application;
import android.content.Context;

import com.taurus.auto.data.prefs.AppPreferenceHelper;
import com.taurus.auto.data.prefs.PreferenceHelper;
import com.taurus.auto.di.ApplicationContext;
import com.taurus.auto.di.PreferenceInfo;
import com.taurus.auto.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application){
        this.application = application;
    }

    @Provides
    @ApplicationContext
    Context providerContext(){
        return application;
    }

    @Provides
    Application provideApplication(){
        return application;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName(){
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferenceHelper providePreferenceHelper(AppPreferenceHelper appPreferenceHelper){
        return appPreferenceHelper;
    }
}
