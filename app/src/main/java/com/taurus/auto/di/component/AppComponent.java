package com.taurus.auto.di.component;

import android.app.Application;
import android.content.Context;

import com.taurus.auto.MvpApp;
import com.taurus.auto.data.network.ApiServiceTaurusAuto;
import com.taurus.auto.data.prefs.PreferenceHelper;
import com.taurus.auto.di.ApplicationContext;
import com.taurus.auto.di.module.ApplicationModule;
import com.taurus.auto.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(MvpApp mvpApp);

    @ApplicationContext
    Context context();

    Application application();

    ApiServiceTaurusAuto apiServiceTaurusAuto();

    PreferenceHelper preferenceHelper();
}
