package com.taurus.auto.di.component;

import com.taurus.auto.di.PerActivity;
import com.taurus.auto.di.module.ActivityModule;
import com.taurus.auto.ui.activities.TitleActivity;
import com.taurus.auto.ui.dialogs.ExitDialog;
import com.taurus.auto.ui.fragments.ChangePasswordFragment;
import com.taurus.auto.ui.fragments.ClientListFragment;
import com.taurus.auto.ui.fragments.ClientsFragment;
import com.taurus.auto.ui.fragments.ClientsInfoFragment;
import com.taurus.auto.ui.fragments.ClientsRoleFragment;
import com.taurus.auto.ui.fragments.EditProfileFragment;
import com.taurus.auto.ui.fragments.EnterPhoneFragment;
import com.taurus.auto.ui.fragments.ForgotPasswordFragment;
import com.taurus.auto.ui.fragments.MainMenuFragment;
import com.taurus.auto.ui.fragments.NewPasswordFragment;
import com.taurus.auto.ui.fragments.ProfileFragment;
import com.taurus.auto.ui.fragments.SignInFragment;
import com.taurus.auto.ui.fragments.SmsCodeFragment;
import com.taurus.auto.ui.fragments.SplashStartFragment;
import com.taurus.auto.ui.presenters.ChangePasswordPresenter;
import com.taurus.auto.ui.presenters.ClientInfoPresenter;
import com.taurus.auto.ui.presenters.ClientsListPresenter;
import com.taurus.auto.ui.presenters.EditProfilePresenter;
import com.taurus.auto.ui.presenters.EnterPhonePresenter;
import com.taurus.auto.ui.presenters.ForgotPasswordPresenter;
import com.taurus.auto.ui.presenters.MainMenuPresenter;
import com.taurus.auto.ui.presenters.NewPasswordPresenter;
import com.taurus.auto.ui.presenters.ProfilePresenter;
import com.taurus.auto.ui.presenters.SignInPresenter;
import com.taurus.auto.ui.presenters.SmsCodePresenter;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashStartFragment splashStartFragment);

    void inject(EnterPhoneFragment enterPhoneFragment);

    void inject(EnterPhonePresenter enterPhonePresenter);

    void inject(SmsCodeFragment smsCodeFragment);

    void inject(SmsCodePresenter smsCodePresenter);

    void inject(SignInFragment signInFragment);

    void inject(SignInPresenter signInPresenter);

    void inject(ForgotPasswordPresenter forgotPasswordPresenter);

    void inject(ForgotPasswordFragment forgotPasswordFragment);

    void inject(NewPasswordFragment newPasswordFragment);

    void inject(NewPasswordPresenter newPasswordPresenter);

    void inject(MainMenuPresenter mainMenuPresenter);

    void inject(MainMenuFragment mainMenuFragment);

    void inject(TitleActivity titleActivity);

    void inject(ExitDialog exitDialog);

    void inject(ChangePasswordFragment changePasswordFragment);

    void inject(ChangePasswordPresenter changePasswordPresenter);

    void inject(ProfilePresenter profilePresenter);

    void inject(ProfileFragment profileFragment);

    void inject(EditProfileFragment editProfileFragment);

    void inject(EditProfilePresenter editProfilePresenter);

    void inject(ClientsFragment clientsFragment);

    void inject(ClientListFragment clientListFragment);

    void inject(ClientsListPresenter clientsListPresenter);

    void inject(ClientInfoPresenter clientInfoPresenter);

    void inject(ClientsInfoFragment clientsInfoFragment);

    void inject(ClientsRoleFragment clientsRoleFragment);
}
